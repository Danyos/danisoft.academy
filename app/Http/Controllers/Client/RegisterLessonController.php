<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use App\Models\Client\CallUpModels;
use App\Models\Client\RegisterLessonModel;
use Illuminate\Http\Request;

class RegisterLessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'tel' => 'required',
            'age' => 'required',
        ]);
        $check = RegisterLessonModel::create($request->all());



        $arr = array('msg' => 'Ինչ-որ բան այն չէ: Խնդրում եմ փորձեք կրկին', 'status' => false);

        if($check){

            $arr = array('msg' => 'Շնորհակալություն ձեր հայտը ուղղարկվել է', 'status' => true);

        }

        return \Illuminate\Support\Facades\Response::Json($arr);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function CallUpCenter(Request $request)
    {
        request()->validate([
            'ClientName' => 'required',
            'phone' => 'required',
        ]);
        $check = CallUpModels::create($request->all());



        $arr = array('msg' => 'Ինչ-որ բան այն չէ: Խնդրում եմ փորձեք կրկին', 'status' => false);

        if($check){

            $arr = array('msg' => 'Շնորհակալություն ձեր հայտը ուղղարկվել է', 'status' => true);

        }

        return \Illuminate\Support\Facades\Response::Json($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
