<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Client\CallUpModels;
use App\Models\Client\ContactUsModels;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function Send(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'tel' => 'required|regex:/[0-9]/|not_regex:/[a-z]/|min:8',
            'email' => 'required|email',
            'comments' => 'required|max:250',
        ]);
        $check = ContactUsModels::create($request->all());



        $arr = array('msg' => 'Ինչ-որ բան այն չէ: Խնդրում եմ փորձեք կրկին', 'status' => false);

        if($check){

            $arr = array('msg' => 'Շնորհակալություն ձեր հայտը ուղղարկվել է', 'status' => true);

        }

        return \Illuminate\Support\Facades\Response::Json($arr);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
