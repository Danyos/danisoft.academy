<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    public function lessons(){

        return view('Lesson.lesson');
    }
    public function lessonMore($data_Lesson="frontend"){

        return view('Lesson.show',compact('data_Lesson'));
    }
    public function start(){
        $data_Lesson='new_start';
        return view('Lesson.newLesson',compact('data_Lesson'));
    }
    public function GiftCard(){
        $data_Lesson='new_start';
        return view('Lesson.GiftCard',compact('data_Lesson'));
    }
    public function lessonRegister($data_Lesson_type="web"){

        return view('Lesson.register',compact('data_Lesson_type'));
    }
     public function price(){

        return view('page.priceList');
    }
}
