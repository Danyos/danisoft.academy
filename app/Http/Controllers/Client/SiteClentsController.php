<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use App\Models\Admin\School\LessonModel;
use App\Models\SliderModel;
use Illuminate\Http\Request;

class SiteClentsController extends Controller
{
    public function index(){

        $sliders=SliderModel::with('Slider_info')->get();
        $lessons=LessonModel::get();

        return view('welcome',compact('sliders','lessons'));
    }
    public function about(){
        return view('page.about');
    }
    public function newstart(){
        return view('page.NewStart');
    }
    public function reviews(){
        return view('page.reviews');
    }
    public function contact(){
        return view('page.contact');
    }
    public function blog(){
        return view('page.blog');
    }


}
