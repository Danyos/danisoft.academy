<?php

namespace App\Http\Controllers\Admin\CallUp;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Models\Client\CallUpModels;
use Illuminate\Http\Request;

class CallUpController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $callUp = CallUpModels::orderBy('id', 'desc')->get();
        $callUpCount = $callUp->count();

        return view('admin.CallUp.index', compact('callUp', 'callUpCount'));

    }

    public function favorite($id)
    {
        $f = CallUpModels::find($id);

        if ($f->favorite == 'active') {
            $f->update([
                'favorite' => 'inactive'
            ]);
        } else {
            $f->update([
                'favorite' => 'active'
            ]);
        }
        return ['id' => $id, 'fav' => $f->favorite];
    }

    public function status($id)
    {
        $f = CallUpModels::find($id);

        if ($f->status == 'active') {
            $f->update([
                'status' => 'inactive'
            ]);
            return ['id' => $id, 'status' => 'inactive'];
        } else {
            $f->update([
                'status' => 'active'
            ]);
            return ['id' => $id, 'status' => 'active'];
        }


    }

    public function show($data)
    {

        if ($data == 'all') {
            $callUp = CallUpModels::orderBy('id', 'desc')->where('client', 'user')->get();

        } elseif ($data == 'inactive') {
            $callUp = CallUpModels::orderBy('id', 'desc')->where('client', 'user')->where('status', 'inactive')->get();

        } else if ($data == 'active') {
            $callUp = CallUpModels::orderBy('id', 'desc')->where('client', 'user')->where('status', 'active')->get();

        } else if ($data == 'fav') {
            $callUp = CallUpModels::orderBy('id', 'desc')->where('client', 'user')->where('favorite', 'active')->get();

        } else if ($data == 'client') {
            $callUp = CallUpModels::orderBy('id', 'desc')->where('client', 'admin')->get();

        }else if ($data == 'clientFavorite') {
            $callUp = CallUpModels::orderBy('id', 'desc')->where('client', 'admin')->where('favorite', 'active')->get();

        }else if ($data == 'clientActive') {
            $callUp = CallUpModels::orderBy('id', 'desc')->where('client', 'admin')->where('status', 'active')->get();

        }else if ($data == 'clientInactive') {
            $callUp = CallUpModels::orderBy('id', 'desc')->where('client', 'admin')->where('status', 'inactive')->get();

        }
        $callUpCount = $callUp->count();
        return view('admin.CallUp.index', compact('callUp', 'callUpCount'));

    }

    public function store(Request $request)
    {
        $callUp = CallUpModels::create($request->all());
        return back();
    }
    public function delete($id)
    {
        $delete = CallUpModels::find($id)->delete();
        return $id;
    }
}
