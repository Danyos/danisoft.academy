<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\School\StudentPriceListMath;
use App\Models\Admin\School\StudentsModel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class HomeController extends AdminController
{
    public function index()
    {

        $LessonPriceAndMath=StudentPriceListMath::where('parent_id',null)->select(
            DB::raw('YEAR(start_date) as year'),
            DB::raw('MONTH(start_date) as month'),
            DB::raw('SUM(price_full) as sum')
        )
            ->groupBy('year', 'month')
            ->get();

        $data = [0,0,0,0,0,0,0,0,0,0,0,0];

        foreach($LessonPriceAndMath as $order){

            $data[$order->month-1] = $order->sum;
        }
        $allprice=StudentPriceListMath::where('parent_id',null)->get()->sum('price_full');

        $student_normal=StudentsModel::where('status','active')->where('level','normal')->get()->count();
        $student_middle=StudentsModel::where('status','active')->where('level','middle')->get()->count();
        $student_progressive=StudentsModel::where('status','active')->where('level','progressive')->get()->count();




        return view('home',compact('data','allprice','student_middle','student_normal','student_progressive'));
    }
}
