<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\School\StudentPriceListMath;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\View;

class AdminController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct()
    {
        $dayToCheck = Carbon::now()->subDays(4);
        $student_price_and_maths=StudentPriceListMath::with(['StudentName'])->where('parent_id',null)->where('month','active')
            ->whereDate("end_date",'<=', $dayToCheck)
            ->select('id','student_id','end_date','start_date')->get();

        View::share('student_price_and_maths',$student_price_and_maths);
    }
}
