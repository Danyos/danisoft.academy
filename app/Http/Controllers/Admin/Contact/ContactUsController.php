<?php

namespace App\Http\Controllers\Admin\Contact;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Models\Client\ContactUsModels;
use Illuminate\Http\Request;

class ContactUsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactUs = ContactUsModels::orderBy('id', 'desc')->get();
        $contactUsCount = $contactUs->count();

        return view('admin.Contact.index', compact('contactUs', 'contactUsCount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($data)
    {
        if ($data == 'all') {
            $contactUs = ContactUsModels::orderBy('id', 'desc')->get();

        } elseif ($data == 'inactive') {
            $contactUs = ContactUsModels::orderBy('id', 'desc')->where('status', 'inactive')->get();

        } else if ($data == 'active') {
            $contactUs = ContactUsModels::orderBy('id', 'desc')->where('status', 'active')->get();

        }
        $contactUsCount = $contactUs->count();
        return view('admin.Contact.index', compact('contactUs', 'contactUsCount'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
