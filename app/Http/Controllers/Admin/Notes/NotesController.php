<?php

namespace App\Http\Controllers\Admin\Notes;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Admin\NotesModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotesController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes=NotesModels::where('user_id',auth()->id())->orderBy('id','desc')->get();
        return view('admin.Notes.index',compact('notes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $note=NotesModels::create([
           'user_id'=>auth()->id(),
           'title'=>$request[0]['title'],
           'description'=>$request[0]['desc'],
           'dateTime'=>$request[0]['date'],

       ]);
       return $note;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notes=NotesModels::where('user_id',auth()->id())->orderBy('id','desc')->where('type',$id)->get();
        return view('admin.Notes.index',compact('notes'));
    } public function fav()
    {
        $notes=NotesModels::where('user_id',auth()->id())->where('favourites','active')->orderBy('id','desc')->get();
        return view('admin.Notes.index',compact('notes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(isset($request['type'])) {
            $notes=NotesModels::find($request['id']);
            $notes->update([
                'type' => $request['type']

            ]);
        }else{

             $notes= NotesModels::find($request['id']);
             if ($notes->favourites==null):
              $notes->update([
                  'favourites' => 'active'

              ]);
             elseif ($notes->favourites=='active'):
                 $notes->update([
                     'favourites' => 'inactive'

                 ]);
             elseif ($notes->favourites=='inactive'):
                 $notes->update([
                     'favourites' => 'active'

                 ]);
                 endif;
        }

        return $notes;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        NotesModels::find($id)->delete();
        return $id;
    }
}
