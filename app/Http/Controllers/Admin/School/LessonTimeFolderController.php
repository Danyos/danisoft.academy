<?php

namespace App\Http\Controllers\Admin\School;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Models\Admin\School\LessonTimeFolder;
use App\Models\Admin\School\StudentPriceListMath;
use App\Models\Admin\School\StudentsModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class LessonTimeFolderController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $folder=LessonTimeFolder::get();
        $folderFirst=LessonTimeFolder::first();
        $student=StudentsModel::with(['StudentPrice'])->where('status','active')->where('group_id',$folderFirst->id)->get();
        $user=User::get();
        $status='all';
        return view('admin.School.LessonTimeList.index',compact('folder','student','folderFirst','user','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateStudent(Request $request)
    {
        $students=StudentsModel::whereIn('id',$request->key)->get();
        foreach ($students as $k=>$student){

            $student->update([
                'group_id'=>$request->folder
            ]);

        }
        return $request->key;




    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $folder=LessonTimeFolder::get();
        $folderFirst=LessonTimeFolder::find($request->folder);
        $student=StudentsModel::where('group_id',$folderFirst->id)->pluck('id')->all();

        return redirect()->route('admin.LessonTimeList.show',encrypt($request->all()));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data=decrypt($id);

        $user=User::get();
        $folder=LessonTimeFolder::get();
        $folderFirst=LessonTimeFolder::find($data['folder']);

        $students=StudentsModel::where('group_id',$folderFirst->id)->where('status','active')->pluck('id')->all();



            if($data['sales']=='all'){

                $student=StudentsModel::with(['StudentPrice'])->where('status','active')->where('group_id',$folderFirst->id)->get();
            }else{
                $studentpriceParent= StudentPriceListMath::where('status',$data['sales'])->pluck('student_id')->all();
                $student=StudentsModel::with(['StudentPrice'])->where('status','active')->where('group_id',$folderFirst->id)->whereIn('id',$studentpriceParent)->get();

            }

        $status=$data['sales'];
        return view('admin.School.LessonTimeList.index',compact('folder','student','folderFirst','user','status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dates =Carbon::today();
        $student = StudentsModel::find($id);


        if(isset($request->priceall)){


            $studentpriceParent= StudentPriceListMath::where('month','active')->where('status','inactive')->where('parent_id',null)->where('student_id',$id)->first();

            $studentprice= StudentPriceListMath::where('month','active')->where('status','inactive')->where('student_id',$id)->get();

            $newdates=Carbon::parse($studentpriceParent->start_date);
            $daysToAdd = 1;
            $date = $newdates->addMonth($daysToAdd);

            foreach ($studentprice as $studentprices){

                $studentprices->update([
                    'status'=>'active',
                    'price'=>$student->price,
                    'day'=>12,
                    'price_full'=>$student->price,
                    'start_date' => Carbon::now()->format('Y-m-d'),
                    'end_date' => $date->format('Y-m-d'),
                    'month'=>'active',
                ]);
            }
            return back()->with('price_debt',["id"=>$student->group_id,"price_debt"=>'Վճարված է']);

        }elseif ($request->price!=null){
        ;

            $studentprice= StudentPriceListMath::where('status','inactive')->where('student_id',$id)->where('parent_id',null)->first();

            $gumar=$student->price/12;
          $day=explode('.',$request->price/$gumar+0.3);


            StudentPriceListMath::create([
                'day'=>$day[0],
                'parent_id'=>$studentprice->id,
                'student_id'=>$id,
                'price'=>$request->price,
                'price_full'=>$request->price+$studentprice->price_full,
                'start_date' => Carbon::now()->format('Y-m-d'),
                'month'=>'inactive',

            ]);
            $j=$request->price+$studentprice->price_full;

            $day2=explode('.',$j/$gumar);
            $daysToAdd = $day2[0]*3;
            $date = $dates->addDays($daysToAdd);

            $studentprice->update([
                'price_full'=>$request->price+$studentprice->price_full,
                'day'=>$day2[0],
                'month'=>'active',
                'end_date' => $date->format('Y-m-d'),
            ]);

            $stud=StudentPriceListMath::where('parent_id',null)->where('status','inactive')->where('student_id',$id)->first();

            if ($stud->price_full >= $student->price) {
                StudentPriceListMath::find($stud->id)->update([
                    'day' => 12,
                    'status' => 'active',
                    'end_date' => $dates->format('Y-m-d'),
                    'month'=>'active',
                ]);

            }

            return back()->with('price_debt',["id"=>$student->group_id,"price_debt"=>'Վճարված է']);

        }else {


            $studentpriceall = StudentPriceListMath::where('student_id', $id)->where('month','active')->where('status', 'inactive')->get();
            $studentprice = StudentPriceListMath::where('student_id', $id)->where('month','active')->where('status', 'inactive')->where('parent_id', '=', null)->first();
            if ($studentprice) {
                $s = $studentprice->price_full;

                if ($s >= $student->price) {
                    foreach ($studentpriceall as $studentprices) {

                        $studentprices->update([
                            'day'=>12,
                            'status' => 'active',
                            'end_date' => $dates->format('Y-m-d'),
                            'month'=>'active',
                        ]);
                    }


                    return back()->with('price_debt',["id" => $student->group_id, "price_debt" => 'Ամիսը լրացավ']);
                } else {

                    $k = $student->price - $s;

                    return back()->with('price_debt',["id" => $student->group_id, "price_debt" => $k]);
                }
            }

        else{
            return back()->with('price_debt',["id" => $student->group_id, "price_debt" => 'Չկա գործողություն']);}
    }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addLesson(Request $request)
    {
      LessonTimeFolder::create($request->all());

        return back();



    }
}
