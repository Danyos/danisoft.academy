<?php

namespace App\Http\Controllers\Admin\School\Reg;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Controller;
use App\Models\Client\List_RegisterLessonStudentResultModel;
use App\Models\Client\RegisterLessonModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class RegisterLessonController extends AdminController
{
    public function show($data)
    {
//        $js=File::get("db/register_lesson_models.json");
//        $arrContent = json_decode($js, true);
//
//        foreach ($arrContent[2]['data'] as $item) {
//
//            \App\Models\Client\RegisterLessonModel::create([
//                "name" => $item["name"],
//                "slug" => $item["slug"],
//                "status" => $item["status"],
//                "age" => $item["age"],
//                "email" => $item["email"],
//                "tel" => $item["tel"],
//                "comments" => $item["comments"],
//            ]);
//        }


        if ($data == 'all') {
            $lessonReg = RegisterLessonModel::orderBy('id', 'desc')->get();
            $lessonRegcount = $lessonReg->count();
        } elseif ($data == 'inactive') {
            $lessonReg = RegisterLessonModel::orderBy('id', 'desc')->where('status', 'inactive')->get();
            $lessonRegcount = $lessonReg->count();
        } else if ($data == 'active') {
            $lessonReg = RegisterLessonModel::orderBy('id', 'desc')->where('status', 'active')->get();
            $lessonRegcount = $lessonReg->count();
        } else if ($data == 'fav') {
            $lessonReg = RegisterLessonModel::orderBy('id', 'desc')->where('favorite', 'active')->get();
            $lessonRegcount = $lessonReg->count();
        } else if ($data == 'student') {
            $lessonReg = RegisterLessonModel::orderBy('id', 'desc')->where('student', 'yes')->get();
            $lessonRegcount = $lessonReg->count();
        }
        return view('admin.School.Reg.index', compact('data', 'lessonReg', 'lessonRegcount'));

    }

    public function info($id){
        $lessonReg = List_RegisterLessonStudentResultModel::orderBy('id', 'desc')->where('reg_id', $id)->get();
        $lessonRegcount = $lessonReg->count();
        $note= RegisterLessonModel::orderBy('id', 'desc')->find($id);
        return view('admin.School.Reg.info', compact('id', 'lessonReg', 'lessonRegcount','note'));

    }
    public function info_store(Request $request){
        request()->validate([
            'comments' => 'required',
        ]);
        $check = List_RegisterLessonStudentResultModel::create($request->all());


        return back();


    }


    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'tel' => 'required',
            'age' => 'required',
        ]);
        $check = RegisterLessonModel::create($request->all());


        return back();
    }

    public function delete($id)
    {
        RegisterLessonModel::find($id)->delete();
        return $id;
    }

    public function favorite($id)
    {
        $f = RegisterLessonModel::find($id);

        if ($f->favorite == 'active') {
            $f->update([
                'favorite' => 'inactive'
            ]);
        } else {
            $f->update([
                'favorite' => 'active'
            ]);
        }
        return ['id' => $id, 'fav' => $f->favorite];
    }
    public function student($id)
    {

        $f = RegisterLessonModel::find($id);

        if ($f->student == 'yes') {
            $f->update([
                'student' => 'no'
            ]);
        } else {
            $f->update([
                'student' => 'yes'
            ]);
        }
        return ['id' => $id, 'student' => $f->student];
    }

    public function status($id)
    {
        $f = RegisterLessonModel::find($id);

        if ($f->status == 'active') {
            $f->update([
                'status' => 'inactive'
            ]);
            return ['id' => $id, 'status' => 'inactive'];
        } else {
            $f->update([
                'status' => 'active'
            ]);
            return ['id' => $id, 'status' => 'active'];
        }


    }
}
