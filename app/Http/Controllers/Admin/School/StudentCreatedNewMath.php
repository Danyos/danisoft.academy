<?php

namespace App\Http\Controllers\Admin\School;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Models\Admin\School\LessonTimeFolder;
use App\Models\Admin\School\StudentPriceListMath;
use App\Models\Admin\School\StudentsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class StudentCreatedNewMath extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $studentprice= StudentPriceListMath::where('status','active')->where('student_id',$id)->get();

        foreach ($studentprice as $studentprices){

            $studentprices->update([
                'month'=>'inactive',
            ]);
        }



        $dates =Carbon::today();
        $daysToAdd =1;
        $date = $dates->addMonth($daysToAdd);
        StudentPriceListMath::create([
            'day' =>0,
            'student_id' => $id,
            'price_full' => 0,
            'start_date' => Carbon::now()->format('Y-m-d'),
            'month'=>'inactive',

        ]);
        return back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data="off";

        $studentPrices=StudentPriceListMath::where('parent_id',$id)->get();
        $studentID=StudentPriceListMath::find($id);
        $student=StudentsModel::find($studentID->student_id);
        return view('admin.School.LessonTimeList.show',compact('studentPrices','student','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
  public function showThisStudent($id)
    {

        $studentId=StudentsModel::find($id);


        $folder=LessonTimeFolder::get();
        $folderFirst=LessonTimeFolder::find($studentId->group_id);
        $student=StudentsModel::where('group_id',$folderFirst->id)->pluck('id')->all();
$data=[
    'sales'=>'all',
    'folder'=>$studentId->group_id,

];
        return redirect()->route('admin.LessonTimeList.show',encrypt($data));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
