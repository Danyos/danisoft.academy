<?php

namespace App\Models\Client;

use Illuminate\Database\Eloquent\Model;

class RegisterLessonModel extends Model
{
    protected $fillable = [
        'slug', 'name', 'status', 'register', 'student', 'age', 'email', 'tel', 'comments','favorite'
    ];
}
