<?php

namespace App\Models\Client;

use Illuminate\Database\Eloquent\Model;

class CallUpModels extends Model
{
    protected $fillable = [
      'ClientName', 'status', 'phone', 'comments','favorite','client'
    ];
}
