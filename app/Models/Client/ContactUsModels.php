<?php

namespace App\Models\Client;

use Illuminate\Database\Eloquent\Model;

class ContactUsModels extends Model
{
    protected $fillable = [
        'how_speak', 'name', 'status', 'email', 'tel', 'comments',
    ];
}
