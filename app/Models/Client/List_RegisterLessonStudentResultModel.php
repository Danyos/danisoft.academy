<?php

namespace App\Models\Client;

use Illuminate\Database\Eloquent\Model;

class List_RegisterLessonStudentResultModel extends Model
{
    protected $fillable = [
       'comments', 'speckDate','reg_id'
    ];
}
