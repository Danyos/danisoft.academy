<?php

namespace App\Models\Admin\School;

use Illuminate\Database\Eloquent\Model;

class LessonTimeFolder extends Model
{
    protected $fillable = [
        'user_id', 'title', 'type', 'user_id'
    ];
}
