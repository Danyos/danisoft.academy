<?php

namespace App\Models\Admin\School;

use Illuminate\Database\Eloquent\Model;

class StudentPriceListMath extends Model
{
    protected $table='student_price_list_maths';
    protected $fillable = [
        'student_id', 'parent_id', 'price_full',
        'day', 'start_date', 'end_date','price',
        'status','month'
    ];
    public function StudentName()
    {
        return $this->hasOne(StudentsModel::class,'id','student_id')->where('status','active');
    }
}
