<?php

namespace App\Models;

use App\Models\Client\SliderInfoModel;
use Illuminate\Database\Eloquent\Model;

class SliderModel extends Model
{
    public function Slider_info()
    {
        return $this->hasOne(SliderInfoModel::class,  'slider_info','id');
    }
}
