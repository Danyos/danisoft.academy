<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionRoleTableSeeder::class,
            UsersTableSeeder::class,
            RoleUserTableSeeder::class,
            LessonTimeFolderSeeder::class,
            StudentsModelsSeeder::class,
            StudentsPriceListMathModels_Seeder::class,
//            Lesson_RegisterSeeder::class,
        ]);
    }
}
