<?php

use    \App\Models\Admin\School\StudentsModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class Lesson_RegisterSeeder extends Seeder
{
    public function run()
    {


        $js=File::get("db/register_lesson_models.json");
        $arrContent = json_decode($js, true);

        foreach ($arrContent[2]['data'] as $item) {

            \App\Models\Client\RegisterLessonModel::create([
                "name" => $item["name"],
                "slug" => $item["slug"],
                "status" => $item["status"],
                "age" => $item["age"],
                "email" => $item["email"],
                "tel" => $item["tel"],
                "comments" => $item["comments"],
            ]);
        }
    }
}
