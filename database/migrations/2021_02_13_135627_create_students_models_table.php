<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_models', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('group_id');
            $table->enum('status',['active','inactive'])->default('active')->comment('Հաձախող ուսանող');
            $table->enum('level',['normal','middle','progressive'])->default('normal')->comment('Ուսանողի մակարդակը');

            $table->string('fullName')->nullable();
            $table->string('email')->nullable();
            $table->string('age')->nullable();
            $table->string('phone')->nullable();
            $table->string('price')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->foreign('group_id')->references('id')->on('lesson_time_folders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_models');
    }
}
