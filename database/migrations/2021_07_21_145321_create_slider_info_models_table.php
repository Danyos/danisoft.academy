<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSliderInfoModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider_info_models', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('slider_info');
            $table->longText('title')->nullable();
            $table->longText('info')->nullable();
            $table->foreign('slider_info')->references('id')->on('slider_models');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slider_info_models');
    }
}
