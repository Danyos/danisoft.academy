<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallUpModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_up_models', function (Blueprint $table) {
            $table->id();
            $table->string('ClientName');
            $table->enum('status',['active','inactive'])->default('inactive');
            $table->enum('favorite',['active','inactive'])->default('inactive');
            $table->enum('client',['admin','user'])->default('user');
            $table->string('phone');
            $table->longText('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_up_models');
    }
}
