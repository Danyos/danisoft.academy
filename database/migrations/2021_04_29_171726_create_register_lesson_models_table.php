<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegisterLessonModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_lesson_models', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->nullable();
            $table->string('name');
            $table->enum('status',['active','inactive'])->default('inactive');
            $table->enum('register',['old','new'])->default('new');
            $table->enum('student',['yes','no'])->default('no');
            $table->enum('favorite',['active','inactive'])->default('inactive');
            $table->string('age');
            $table->string('email');
            $table->string('tel');
            $table->longText('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_lesson_models');
    }
}
