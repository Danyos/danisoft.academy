<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListRegisterLessonStudentResultModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list__register_lesson_student_result_models', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('reg_id');
            $table->foreign('reg_id')->references('id')->on('register_lesson_models');
            $table->longText('comments');
            $table->date('speckDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list__register_lesson_student_result_models');
    }
}
