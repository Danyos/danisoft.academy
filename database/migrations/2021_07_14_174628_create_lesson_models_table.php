<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_models', function (Blueprint $table) {
            $table->id();
            $table->string('sale')->nullable();
            $table->string('title');
            $table->string('slug')->nullable();
            $table->string('avatar')->nullable();
            $table->string('math')->nullable();
            $table->longtext('description');
            $table->enum('lang',['am','ru','en'])->default('am');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_models');
    }
}
