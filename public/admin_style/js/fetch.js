function fetchsend(token,url,method,data,dataName){
    fetch(url, {
        method: method,
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            "X-CSRF-Token": token
        }
    }).then(function(response){
        return response.json();
    })  .then(function(json){
        console.log(json)
        window[dataName](json);
    })
        .catch(function(error){

            console.log(error)
        });
}
