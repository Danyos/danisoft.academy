<section class="course-start-time-area pd">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-1 text-center">
                <div class="course-start-timing">
                    <div class="course-start-text">
                        <h2>Գրանցվեք մեր նոր դասընթացին </h2>
                        <h5>WEB Ծրագրավորում 0-ից</h5>

                        <p style="text-align: justify;">Գրանցվեք մեկ անվճար փորձնական դասի և պատկերացում կազմեք
                            ծրագրավորման մասին, ստանալով Ձեզ հետաքրքրող բոլոր հարցերը, Դա կօգնի ավելի ճիշտ
                            կողմնորոշվել
                            դասընթացների և մասնագիտության ընտրության հարցում</p>
                    </div>
                    <div class="timer" id="countdown"></div>
                    <p>ՕՆԼԱՅՆ/ՕՖՖԼԱՅՆ ԴԱՍԸՆԹԱՑ
                    <p> Դասընթացը բաղկացած է 12 հանդիպումներից, որոնք անցկացվելու է DaniSoft ակադեմիայի կողմից:
                        Դասընթացի մեկնարկը տեղի կունենա</p>


                </div>
            </div>
            <div class="col-md-5 col-sm-6 col-sm-offset-3 col-md-offset-1">
                <div class="sign-up-form">
                    <h2>Գրանցվել</h2>
                    <div class="alert alert-success" id="msg_Action_Register">
                                <span  class="uppercase">Դուք գրանցվել եք դասընթացին
                                մեր մասնագետը կապ կհասատի Ձեզ հետ</span>
                    </div>
                    <form id="lessonActionRegister" method="post" action="{{route('client.lessonRegister')}}">
                        @csrf
                        <div class="form-group">

                            <div class="field">
                                <input type="text" class="form-control" placeholder="Անուն Ազգանուն" aria-label="Username"
                                       aria-describedby="user-name" name="name" id="formGroupExampleInput">
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">

                            <div class="field">
                                <input type="number" min="10" class="form-control" placeholder="Տարիք" required=""  name="age">
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">

                            <div class="field">
                                <input type="tel" name="tel" class="form-control" placeholder="0 ** *** ***"
                                       aria-label="phone" aria-describedby="user-phone-number">
                            </div>
                            <input type="hidden" id="dataLesson" name="slug">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">

                            <div class="field">
                                <input type="email" class="form-control" placeholder="example@gmail.com"
                                       aria-label="email" aria-describedby="user-mail"  name="email" id="email">
                            </div>
                            <input type="hidden" id="dataLesson" name="slug">
                            <span class="help-block"></span>
                        </div>


                        <button type="submit"  id="send_form_action" class="btn btn-success btn-block ">Հաստատել</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
