<section class="container">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-7">
            <div class="section-text-content">
                <h1>ԾՐԱԳՐԱՎՈՐՄԱՆ ԴԱՍԸՆԹԱՑՆԵՐ</h1>
                <hr>
                <p>
                    Չգիտե՞ք, ինչ դասընթաց ընտրել, թողե՛ք Ձեր կոնտակտային տվյալները և մեր մասնագետները կապ
                    կհաստատեն
                    Ձեզ հետ և կիրականացնեն խորհրդատվություն
                </p>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-lg-5">
            <div class="section-sign-in-content">
                <div class="sign-in">
                    <div class="sign-in-header">
                        <h3>ՊԱՏՎԻՐԵՔ ՀԵՏԱԴԱՐԱՁ ԶԱՆԳ</h3>
                        <p>
                            ԹՈՂԵ՛Ք ՁԵՐ ՀԵՌԱԽՈՍԱԱՀԱՄԱՐԸ, Եւ ՄԵՆՔ ՁԵԶ ՀԵՏ ԿԱՊ ԿՀԱՍՏԱՏԵՆՔ ՀՆԱՐԱՎՈՐԻՆՍ ԿԱՐՃ
                            ԺԱՄԿԵՏՈՒՄ
                        </p>
                    </div>
                    <div class="sign-in-footer">
                        <form action="" method="post">
                            <div class="input-group mb-3">
                                <input class="form-control" type="text" placeholder="Անուն Ազգանուն"
                                       aria-label="default input example" required>
                            </div>

                            <div class="input-group mb-3">
                                <input class="form-control" type="tel" placeholder="Հեռախոսահամար"
                                       aria-label="default input example" required>
                            </div>

                            <div class="input-group mb-3">
                                <button type="button" class="btn  btn-secondary confirm-button">Պատվիրել զանգ</button>
                            </div>
                        </form>
                        <p class="sign-in-information">
                            Խնդրում ենք հաստատումից առաջ ստուգել Ձեր կողմից լրացված տվյալները
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

