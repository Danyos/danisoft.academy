
<section id="current" class="current-window wow fadeInUp" data-wow-delay="0.5s">
    <div class="container">
        <h1 class="text-center h-m-header-name">ԱՌԱՋԻԿԱ և ԵՎ ՆԵՐԿԱՅԻՍ ԴԱՍԸՆԹԱՑՆԵՐԻ ՄԱՍԻՆ</h1>
        <h6 class="text-center h-m-second-name" style="color:black">ԶԱՐԳԱՑՐԵ՛Ք ՁԵՐ ՀՄՏՈՒԹՅՈՒՆՆԵՐԸ, ՁԵ՛ՌՔ ԲԵՐԵՔ ԹՐԵՆԴԱՅԻՆ
            ՄԱՍՆԱԳԻՏՈՒԹՅՈՒՆ</h6>
        <div class="row">
            <div class="col-12 col-sm-6 co-md-4 col-lg-4">
                <div class="current-item">
                    <div class="current-image-window">
                        <img src=" {{asset('clent/images/lesson/web0.jpg')}}" alt="full-stack" class="current-image">
                    </div>
                    <div class="current-content">
                        <h5 class="current-name">ՎԵԲ ԾՐԱԳՐԱՎՈՐՈՒՄԸ 0-ԻՑ</h5>
                        <p class="current-description">Պատրա՞ստ ես սկսելու քո ծրագրավորման
                            ուղին, դառնալ պահանջված ծրագրավորող ,ապա մեր  դասընթացը  հենց քեզ համար է</p>
                        <p class="current-view-more">
                                                                   <button onclick="javascript: window.location.href = '{{route('client.lessonMore','web0')}}'" type="button" class="btn btn-light current-button myMoreBtn">Իմանալ ավելին</button>
                            <span class="padding-10"> </span>
                            <button type="button" class="btn myButton confirm-button-coltuaction" data-toggle="modal" data-target="#signInModal"
                                    onclick="document.getElementById('dataLesson').value='web0'" >Գրանցվել</button>

                        </p>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-6 co-md-4 col-lg-4">
                <div class="current-item">
                    <div class="current-image-window">
                        <img src="{{asset('clent/images/lesson/JavaScript.jpg')}}" alt="java-script" class="current-image">
                    </div>
                    <div class="current-content">
                        <h5 class="current-name">Javascript դասընթաց</h5>
                        <p class="current-description">Դասընթացը սկսելու համար ձեզ հարկավոր չէ JavaScript-ի նախնական գիտելիքներ:
                        </p>
                        <p class="current-view-more">
                            {{--                                    <button onclick="javascript: window.location.href = '{{route('client.lessonMore','js')}}'" type="button" class="btn btn-light current-button myMoreBtn">Իմանալ ավելին</button>--}}
                            <span class="padding-10"> </span>
                            <button type="button" class="btn myButton confirm-button-coltuaction" data-toggle="modal" data-target="#signInModal"
                                    onclick="document.getElementById('dataLesson').value='JS'" >Գրանցվել</button>





                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 co-md-4 col-lg-4">
                <div class="current-item">
                    <div class="current-image-window">
                        <img src="{{asset('clent/images/lesson/Recat.JS.jpg')}}" alt="react-js" class="current-image">
                    </div>
                    <div class="current-content">
                        <h5 class="current-name">React js դասընթաց</h5>
                        <p class="current-description">React js-ը պարզապես սիրված չէ.  Այլ ապագա է  կարող են մասնակցել այն անձիք ովքերն ունեն Javascript բավարար գիտելիք</p>
                        <p class="current-view-more">
                        <!--<button  onclick="javascript: window.location.href = '{{route('client.lessonMore','Reactjs')}}'" type="button" class="btn btn-light current-button myMoreBtn">Իմանալ ավելին</button>-->
                            <span class="padding-10"> </span>
                            <button type="button" class="btn myButton confirm-button-coltuaction" data-toggle="modal" data-target="#signInModal"
                                    onclick="document.getElementById('dataLesson').value='react js'" >Գրանցվել</button>

                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 co-md-4 col-lg-4">
                <div class="current-item">
                    <div class="current-image-window">
                        <img src="{{asset('clent/images/lesson/php.jpg')}}" alt="back-end" class="current-image">
                    </div>
                    <div class="current-content">
                        <h5 class="current-name">PHP դասընթաց</h5>
                        <p class="current-description">Բացի այն, որ PHP-ը շատ հեշտ է սովորել։
                            Դասընթացը սկսելու համար ձեզ հարկավոր չէ PHP-ի նախնական գիտելիքներ:</p>
                        <p class="current-view-more">
                            {{--                              <button onclick="javascript: window.location.href = '{{route('client.lessonMore','js')}}'" type="button" class="btn btn-light current-button myMoreBtn">Իմանալ ավելին</button>--}}
                            <span class="padding-10"> </span>
                            <button type="button" class="btn myButton confirm-button-coltuaction" data-toggle="modal" data-target="#signInModal"
                                    onclick="document.getElementById('dataLesson').value='Laravel'" >Գրանցվել</button>

                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 co-md-4 col-lg-4">
                <div class="current-item">
                    <div class="current-image-window">
                        <img src="{{asset('clent/images/lesson/English.jpg')}}" alt="react-js" class="current-image">
                    </div>
                    <div class="current-content">
                        <h5 class="current-name">English դասընթաց</h5>
                        <p class="current-description"> Կաշկանդվու՞մ ես անգլերեն խոսել,իսկ  աշխատանքում դա կարևոր պայման է</p>
                        <p class="current-view-more">
                        <!--<button  onclick="javascript: window.location.href = '{{route('client.lessonMore','english')}}'" type="button" class="btn btn-light current-button myMoreBtn">Իմանալ ավելին</button>-->
                            <span class="padding-10"> </span>
                            <button type="button" class="btn myButton confirm-button-coltuaction" data-toggle="modal" data-target="#signInModal"
                                    onclick="document.getElementById('dataLesson').value='English'" >Գրանցվել</button>

                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 co-md-4 col-lg-4">
                <div class="current-item">
                    <div class="current-image-window">
                        <img src=" {{asset('clent/images/lesson/poqrik_cragravorox.jpg')}}" alt="node-js" class="current-image">
                    </div>
                    <div class="current-content">
                        <h5 class="current-name">«Ապագա Փոքրիկ ծրագրավորող»</h5>
                        <p class="current-description"> «Ապագա Փոքրիկ ծրագրավորող»  դասընթացը՝ 𝟭𝟯-𝟭𝟲տ երեխաների համար
                        </p>
                        <p class="current-view-more">
                            {{--                                 <button onclick="javascript: window.location.href = '{{route('client.lessonMore','js')}}'" type="button" class="btn btn-light current-button myMoreBtn">Իմանալ ավելին</button>--}}
                            <span class="padding-10"> </span>
                            <button type="button" class="btn myButton confirm-button-coltuaction" data-toggle="modal" data-target="#signInModal"
                                    onclick="document.getElementById('dataLesson').value='Child'" >Գրանցվել</button>

                        </p>
                    </div>
                </div>
            </div>

        </div>
        <br>

    </div>
    <div class="coll-in-parallax">
        <h2 class="mystyleh3 coltuacionBanertitle" style="">Ծանոթանալ մեկնարկող դասընթացի մասին</h2>
        <br>
        <a  href="{{route('client.new_start')}}"class="btn btn-dark coll-in-parallax-button confirm-button-coltuaction">
           ԻՄԱՆԱԼ ԱՎԵԼԻՆ</a>
    </div>
</section>
