<div class="modal fade" id="CallUpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Գրանցվել դասընթացին</h5>
                <button type="button" class="close btn" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <i class="fas fa-times"></i>
                        </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-success d-none" id="msg_call_up_popUP">
            <span id="res_message">Դուք գրանցվել եք մեր մասնագետը կապ կհասատի Ձեզ հետ</span>
                </div>
                <form id="callUp" method="get" action="{{route('client.CallUpCenter')}}">
                    @csrf
                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="modal-icon-window input-group-text" id="user-name" title="Անուն Ազգանուն"><i class="far fa-user"></i></span>

                        </div>
                        <span class="text-danger"></span>
                        <div class="field">
                            <input type="text" class="form-control" placeholder="Անուն Ազգանուն" aria-label="Username"
                                   aria-describedby="user-name" name="ClientName" id="formGroupExampleInput">
                        </div>
                    </div>

                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-append">
                            <span class="modal-icon-window input-group-text" id="user-phone-number" title="Հեռախոսահամար"><i class="fas fa-phone-alt"></i></span>
                        </div>
                        <span class="text-danger"></span>
                        <div class="field">
                            <input class="form-control" type="number" name="phone" min="8" placeholder="Հեռախոսահամար" >

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn myButton btn-outline-secondary" id="send_forms">Հաստատել</button>
                    </div>
                </form>
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeModals" hidden>Close</button>
        </div>
    </div>
</div>
