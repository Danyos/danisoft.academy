
<section class="lecturera-area pd-top-100 pd-bottom-70 section-bg " id="teacher">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">Դասընթացները վարում են</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="single-lecturer-item">
                    <div class="single-lecturer-img">
                        <img src="{{asset('client/assets/images/teacher/Daniel_Hambardzumyan.jpg')}}" alt=""
                             style="object-fit: cover;width: 100%;">
                        <div class="lecturer-overlay">

                            <p>Full Stack</p>
                        </div>
                    </div>
                    <div class="lecturer-desc">
                        <h4>Դանիել Համբարձումյան <span></span></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="single-lecturer-item">
                    <div class="single-lecturer-img">
                        <img src="{{asset('client/assets/images/teacher/David_Gyulnazaryan.jpg')}}" alt=""
                             style="object-fit: cover;width: 100%;">
                        <div class="lecturer-overlay">
                            <span>Full stack developer</span></h4>
                        </div>
                    </div>
                    <div class="lecturer-desc">
                        <h4>Դավիթ Գյուլնազարյան</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="single-lecturer-item">
                    <div class="single-lecturer-img">
                        <img src="{{asset('client/assets/images/teacher/Hayk_Malxasyan.jpg')}}" alt=""
                             style="object-fit: cover;width: 100%;">
                        <div class="lecturer-overlay">

                            <p>Frontend developer</p>
                        </div>
                    </div>
                    <div class="lecturer-desc">
                        <h4>Հայկ Մալխասըան</h4>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
