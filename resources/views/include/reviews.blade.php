<section class="testimonial-chooce-area pd">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="section-title">ԿԱՐԾԻՔՆԵՐ</h2>
                <div class="tastimonial-wrapper">
                    <div class="tastimonial-item-area">
                        <div class="single-testimonial">
                            <p>
                                DaniSoft առավել ևս Դանիել Համբարձումյան շատ շնորհակալեմ ձեր տված ամենօրյա
                                գիտելիքների և
                                փորձի համար😊

                                <i class="fa fa-quote-left"></i>
                            </p>
                        </div>
                        <div class="testimonial-author">
                            <img src="{{asset('client/assets/images/reviews/Ararat.jpg')}}" alt="">
                            <h4>Ararat Virapyan <span>Մեր ուսանող</span></h4>
                        </div>
                    </div>
                    <div class="tastimonial-item-area">
                        <div class="single-testimonial">
                            <p>Իսկապես որ հրաշալի մասնագետների խումբ։Անձամբ առիթ ունեցա ուսանելու ձեզ մոտ շատ
                                շնորհակալ
                                եմ։ Անհատական մոտեցում է ցուցաբերվում յուրաքանչյուրին։
                                🥰🥰🥰🥰🥰Ձեզ նորանոր հաջողություններ եմ մաղթում։
                                <i class="fa fa-quote-left"></i>
                            </p>
                        </div>
                        <div class="testimonial-author">
                            <img
                                src="https://scontent.fevn6-1.fna.fbcdn.net/v/t1.6435-9/135934421_2768450963417069_5259192798491071605_n.jpg?_nc_cat=103&ccb=1-3&_nc_sid=09cbfe&_nc_ohc=QNURg-rALSwAX9_MwkO&_nc_ht=scontent.fevn6-1.fna&oh=2d9b5dbae088965ef3f0a50a475ff923&oe=60E002ED"
                                alt="">
                            <h4>Շուշան Հարությունյան <span>Մեր ուսանող</span></h4>
                        </div>
                    </div>
                    <div class="tastimonial-item-area">
                        <div class="single-testimonial">
                            <p>
                                DaniSoft I found through my friend a programmer with experience for 4 years.
                                Teachers
                                are professionals here. Their approach is different, different for everyone.
                                <i class="fa fa-quote-left"></i>
                            </p>
                        </div>
                        <div class="testimonial-author">
                            <img src="{{asset('client/assets/images/reviews/Colak.jpg')}}" alt="">
                            <h4>Colak Nersisyan <span>Մեր ուսանող</span></h4>
                        </div>
                    </div>
                    <div class="tastimonial-item-area">
                        <div class="single-testimonial">
                            <p>

                                Ես ինձ հաջողակ եմ համարում, քանի որ հնարավորություն ունեմ սովորել նման միջավայրում,
                                նման
                                մասնագետների մոտ։ Հուսով եմ, որ արժանի եմ ուսանել այդ այստեղ, մեծ երանություն է այս
                                մեծ
                                ընտանիքի մի մասնիկը լինել։ Շնորհակալ եմ անչափ տրված գիտելիքների, ընտիրագույն
                                մթնոլորտի
                                ու բովանդակալից հանդիպումների համար։
                                😊😊😊😊

                                <i class="fa fa-quote-left"></i>
                            </p>
                        </div>
                        <div class="testimonial-author">
                            <img src="{{asset('client/assets/images/reviews/Meline.jpg')}}" alt="">
                            <h4>Meline <span>Մեր ուսանող</span></h4>
                        </div>
                    </div>
                    <div class="tastimonial-item-area">
                        <div class="single-testimonial">
                            <p>

                                Thank you for your high quality classes


                                <i class="fa fa-quote-left"></i>
                            </p>
                        </div>
                        <div class="testimonial-author">
                            <img src="{{asset('client/assets/images/reviews/Hakob.jpg')}}" alt="">
                            <h4>HAKOB MKOYAN <span>Մեր ուսանող</span></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h2 class="section-title">ԻՆՉՈՒ՞ ԸՆՏՐԵԼ DaniSoft</h2>
                <div class="accordion accordion-box-4">
                    <div class="panel-group" id="accordion1">
                        <div class="panel panel-default">
                            <div class="panel-heading active">
                                <h3 class="panel-title">
                                    <a class="accordion-toggle open collapsed" data-toggle="collapse"
                                       data-parent="#accordion1" href="#collapseOne1" aria-expanded="false">
                                        1. ԺԱՄԱՆԱԿԱԿԻՑ ԾՐԱԳԻՐ
                                        <i class="fa fa-caret-down pull-right"></i>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapseOne1" class="panel-collapse collapse in" aria-expanded="false">
                                <div class="panel-body">
                                    <p>
                                        Դասընթացի ծրագիրը կառուցված է շուկայի պահանջներին համապատասխան և պարբերաբար
                                        թարմացվում է:
                                        Մենք ենք Ձեզ այն, ինչ հարկավոր է լավ մասնագետ դառնալու համար առանց ավելորդ
                                        անգամ
                                        Ձեզ
                                        ծանրաբեռնելու:
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse"
                                       data-parent="#accordion1" href="#collapseTwo1" aria-expanded="false">
                                        2. 100% ԳՈՐԾՆԱԿԱՆ ԴԱՍԸՆԹԱՑ
                                        <i class="fa fa-caret-down pull-right"></i>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapseTwo1" class="panel-collapse collapse" style="height: 0px;"
                                 aria-expanded="false">
                                <div class="panel-body">
                                    <p> Ուսուցման գործընթացը հիմնված է գործնական խնդիրների և ուսուցչի հետ շփման վրա:
                                        Մենք միանման
                                        դասախոսություններ չունենք: Եվ գիտելիքն անմիջապես գործնականում ամրագրված
                                        է:</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse"
                                       data-parent="#accordion1" href="#collapseThree1" aria-expanded="false">
                                        3. ՆՎԵՐ ՔԱՐՏ, ԶԵՂՉԵՐ
                                        <i class="fa fa-caret-down pull-right"></i>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapseThree1" class="panel-collapse collapse" style="height: 0px;"
                                 aria-expanded="false">
                                <div class="panel-body">
                                    <p>
                                        Դառնալով DaniSoft-ի ուսանող կստանաք 20% զեղչի քարտ «DaniSoft»-ի մյուս
                                        դասընթացների համար
                                        ինչպես նաև 10% զեղչի քարտ, որը կարող ես նվիրել մտերիմներիդ մեր դասընթացներին
                                        մասնակցելու համար։
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse"
                                       data-parent="#accordion1" href="#collapseFoure1" aria-expanded="false">
                                        4. ՀԱՎԵԼՅԱԼ ՆՎԵՐ ՎԵԲԻՆԱՐ SMM ԲԻԶՆԵՍԻ ՀԱՄԱՐ
                                        <i class="fa fa-caret-down pull-right"></i>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapseFoure1" class="panel-collapse collapse" style="height: 0px;"
                                 aria-expanded="false">
                                <div class="panel-body">
                                    <p>
                                        Վեբինարի ընթացքում կխոսվի Թվային մարքեթինգի դերը բիզնեսի զարգացման գործում
                                        թեմայից:
                                        Դուք ձեռք կբերեք հմտություններ, խորհուրդներ որոնք կօգնեն Ձեր ընկերությանը
                                        բիզնեսի զարգացման գործում:
                                        Նաև, թե ինչպիսի սխալներից է հարկավոր խուսափել թվային մարքեթինգում, ինչն է
                                        լավ
                                        աշխատում, իսկ ինչը՝ ոչ այնքան:
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse"
                                       data-parent="#accordion1" href="#collapseFoure60" aria-expanded="false">
                                        5. ԱՅԼԸՆՏՐԱՆՔԻ ԲԱՑԱԿԱՅՈՒԹՅՈՒՆ
                                        <i class="fa fa-caret-down pull-right"></i>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapseFoure60" class="panel-collapse collapse" style="height: 0px;"
                                 aria-expanded="false">
                                <div class="panel-body">
                                    <p>

                                        Մասնակցելով մեր կողմից անցկացվող դասընթացներին՝ Դուք դառնում եք DaniSoft
                                        Academy-ի ակադեմիայի անդամ ինչը նշանակում է դուք ստանում եք ավելին`
                                        ընդգրկվում
                                        եք մեր փակ խումբ, ստանում հավելյալ բոնուսներ նյութեր, խորհուրդներ,
                                        վիդեոդասեր,
                                        վեբինարներ որոնք առավել արդյունավետ կդարձնեն այն գիտելիքը, որ ստացել ենք:

                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse"
                                       data-parent="#accordion1" href="#collapseFoure20" aria-expanded="false">
                                        6. ՎԿԱՅԱԿԱՆԻ ՀԱՆՁՆՈՒՄ
                                        <i class="fa fa-caret-down pull-right"></i>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapseFoure20" class="panel-collapse collapse" style="height: 0px;"
                                 aria-expanded="false">
                                <div class="panel-body">
                                    <p>


                                        Շրջանավարտները ստանում են նմուշի վկայական դասընթացի հաջող ավարտից հետո։


                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <span class="btn-Register pd-top-100" data-toggle="modal" data-target="#signInModal"
                  onclick="document.getElementById('dataLesson').value='PREMIUM_Offline'">
                                 <a href="#" class="mybtn-two">
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                        Գրանցվել Հիմա
                                 </a>
                  </span>

        </div>
    </div>
</section>
