<section id="contact" class="contact-us">
    <div class="container">
        @isset($data_Lesson)
        <h1 class="text-center h-m-header-name">ԿԱՐՈՂ ԵՔ ՈՒՂԵԼ ՁԵՐ ՀԱՐՑԸ WEB0 ԴԱՍԸՆԹԱՑԻ ՎԵՐԱԲԵՐՅԱԼ</h1>
            @else
        <h1 class="text-center h-m-header-name">ՀԵՏԱԴԱՐՁ ԿԱՊ</h1>
        @endisset
        <div class="form-window">

                <div class="alert alert-success d-none" id="msg_contact">
                    <span id="contact_message">Դուք գրանցվել եք մեր մասնագետը կապ կհասատի Ձեզ հետ</span>
                </div>
                <form id="send_MSG" method="get" action="{{route('client.sendMSG')}}">
                    @csrf
                <div class="row">
                    <div class="col-12 mb-3">
                        <div class="form-group field">
                            <label class="w-100">
                             <input required type="text" name="name" class="form-control"
                                                        placeholder="Անուն ազգանուն*"></label>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-6 mb-3">
                        <div class="form-group field">
                            <label class="w-100"><input type="email" name="email" class="form-control"
                                                        placeholder="Էլ․ հասցե"></label>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-6 mb-3">
                        <div class="form-group field">
                            <label class="w-100"><input type="number" name="tel" class="form-control"
                                                        placeholder="Հեռախոսահամար*"></label>
                        </div>
                    </div>
                    <div class="col-12 mb-3">
                        <div class="form-group field">
                            <label class="w-100">
                                <textarea class="form-control" rows="3"
                                                           placeholder="Բովանդակություն" name="comments"></textarea></label>
                        </div>
                    </div>
                    <div class="col-12 mb-3">
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-secondary w-100 confirm-button-coltuaction" style="text-transform: uppercase" id="send_forms">Ուղարկել</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
