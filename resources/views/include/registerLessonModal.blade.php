<div class="modal fade" id="signInModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">ԳՐԱՆՑՎԵՔ  <b>ՄԵՐ ՆՈՐ </b>ԴԱՍԸՆԹԱՑԻՆ</h4>

                    <button type="button" class="close btn" data-dismiss="modal" aria-label="Close"
                            style="position: absolute;right: 19px; top: 16px;" id="closeModal">
                        <span aria-hidden="true">
                            <i class="fa fa-times"></i>
                        </span>
                    </button>

            </div>
            <div class="alert alert-success d-none-js" id="msg_div">
                                <span  class="uppercase">Դուք գրանցվել եք դասընթացին
                                մեր մասնագետը կապ կհասատի Ձեզ հետ</span>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="well">

                            <form id="lessonRegister" method="post" action="{{route('client.lessonRegister')}}">
                                @csrf
                                <div class="form-group">

                                    <div class="field">
                                         <input type="text" class="form-control" placeholder="Անուն Ազգանուն" aria-label="Username"
                                           aria-describedby="user-name" name="name" id="formGroupExampleInput">
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">

                                    <div class="field">
                                        <input type="number" min="10" class="form-control" placeholder="Տարիք" required=""  name="age">
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">

                                    <div class="field">
                                        <input type="tel" name="tel" class="form-control" placeholder="0 ** *** ***"
                                               aria-label="phone" aria-describedby="user-phone-number">
                                    </div>
                                    <input type="hidden" id="dataLesson" name="slug">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">

                                    <div class="field">
                                        <input type="email" class="form-control" placeholder="example@gmail.com"
                                               aria-label="email" aria-describedby="user-mail"  name="email" id="email">
                                    </div>
                                    <input type="hidden" id="slugData" name="slug">
                                    <span class="help-block"></span>
                                </div>


                                <button type="submit" value="login" id="send_form" name="submit" class="btn btn-success btn-block " form="lessonRegister">Հաստատել</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <p class="lead">Ի՞ՆՉ ԿՏԱ  ՔԵԶ <span class="text-success">ԴԱՍԸՆԹԱՑԸ</span></p>
                        <ul class="list-unstyled" style="line-height: 1.8">

                            <li><span class="fa fa-check text-success"></span> Ամբողջովին պրակտիկ դասընթաց</li>
                            <li><span class="fa fa-check text-success"></span> Ֆունկցիոնալ կայքի ստեղծում </li>
                            <li><span class="fa fa-check text-success"></span> 20% զեղչի հնարավորություն մեր մյուս դասընթացներից օգտվելու դեպքում</li>
                            <li><span class="fa fa-check text-success"></span> 10 % զեղչ մտերիմիդ համար</li>
                            <li><span class="fa fa-check text-success"></span> Հետադարձ կապի ապահովում`ուսանող-դասախոս</li>
                            <li><span class="fa fa-check text-success"></span> Սերտիֆիկացում</li>
                            <li><span class="fa fa-check text-success"></span> Աշխատանք</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
