<section class="why-choose-area pd reg-lessons" id="priceList" style="background:#17253F;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title ">ՓԱԹԵԹՆԵՐ</h2>


            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="tab-item-list">
                    <li class="active"><a data-toggle="tab" href="#tab-list-1" aria-expanded="true"><i
                                class="fa fa-book"></i><span>Offline</span></a></li>
                    <li class=""><a data-toggle="tab" href="#tab-list-2" aria-expanded="false"><i
                                class="fa fa-headphones"></i><span>Online</span></a></li>

                </ul>
                <h6 class="text-center h-m-second-name">ԸՆՏՐԵՔ ՁԵԶ ՀԱՐՄԱՐ</h6>
                <div class="row">
                    <div class="tab-content tab-content aboutOur">
                        <div role="tabpanel" class="tab-pane active" id="tab-list-1">

                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="reg-lessons-item">
                                    <h4 class="reg-lessons-item-name">BASIC</h4>
                                    <h1 class="reg-lessons-item-price">35․500 դրամ</h1>
                                    <h6 class="reg-lessons-item-terms">Դասընթացի պայմանները.</h6>
                                    <hr>
                                    <ul>
                                        <li>Ամիս 12 դաս</li>
                                        <li>Շաբաթական 3 օր</li>
                                        <li>Դասաժամ 2 ժամ</li>
                                        <li>Մասնակիցները խմբում 5</li>
                                    </ul>
                                    <p class="reg-lessons-item-button-window">
                               <span class="btn-Register" data-toggle="modal" data-target="#signInModal"
                                     onclick="document.getElementById('dataLesson').value='PREMIUM_Offline'">
                                 <a href="#" class="mybtn-two">
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                        Գրանցվել
                                 </a>
                              </span>
                                    </p>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="reg-lessons-item">
                                    <h4 class="reg-lessons-item-name">STANDARD</h4>
                                    <h1 class="reg-lessons-item-price">55․500 դրամ</h1>
                                    <h6 class="reg-lessons-item-terms">Դասընթացի պայմանները.</h6>
                                    <hr>
                                    <ul>
                                        <li>Ամիս 12 դաս</li>
                                        <li>Շաբաթական 3 օր</li>
                                        <li>Դասաժամ 2 ժամ</li>
                                        <li>Մասնակիցները խմբում 2</li>
                                    </ul>
                                    <p class="reg-lessons-item-button-window">
                              <span class="btn-Register" data-toggle="modal" data-target="#signInModal"
                                    onclick="document.getElementById('dataLesson').value='PREMIUM_Offline'">
                                 <a href="#" class="mybtn-two">
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                        Գրանցվել
                                 </a>
                              </span>
                                    </p>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="reg-lessons-item">
                                    <h4 class="reg-lessons-item-name">PREMIUM</h4>
                                    <h1 class="reg-lessons-item-price">85․500 դրամ</h1>
                                    <h6 class="reg-lessons-item-terms">Դասընթացի պայմանները.</h6>
                                    <hr>
                                    <ul>
                                        <li>Ամիս 12 դաս</li>
                                        <li>Շաբաթական 3 օր</li>
                                        <li>Դասաժամ 2 ժամ</li>
                                        <li>Մասնակիցները խմբում 1</li>
                                    </ul>
                                    <p class="reg-lessons-item-button-window">

                              <span class="btn-Register" data-toggle="modal" data-target="#signInModal"
                                    onclick="document.getElementById('dataLesson').value='PREMIUM_Offline'">
                                 <a href="#" class="mybtn-two">
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                        Գրանցվել
                                 </a>
                              </span>
                                    </p>
                                </div>
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab-list-2">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="reg-lessons-item">
                                    <h4 class="reg-lessons-item-name">BASIC</h4>
                                    <h1 class="reg-lessons-item-price">30․300 դրամ</h1>
                                    <h6 class="reg-lessons-item-terms">Դասընթացի պայմանները.</h6>
                                    <hr>
                                    <ul>
                                        <li>Ամիս 12 դաս</li>
                                        <li>Շաբաթական 3 օր</li>
                                        <li>Դասաժամ 2 ժամ</li>
                                        <li>Մասնակիցները խմբում 5</li>
                                    </ul>
                                    <p class="reg-lessons-item-button-window">
                               <span class="btn-Register" data-toggle="modal" data-target="#signInModal"
                                     onclick="document.getElementById('dataLesson').value='PREMIUM_Offline'">
                                 <a href="#" class="mybtn-two">
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                        Գրանցվել
                                 </a>
                              </span>
                                    </p>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="reg-lessons-item">
                                    <h4 class="reg-lessons-item-name">STANDARD</h4>
                                    <h1 class="reg-lessons-item-price">50․500 դրամ</h1>
                                    <h6 class="reg-lessons-item-terms">Դասընթացի պայմանները.</h6>
                                    <hr>
                                    <ul>
                                        <li>Ամիս 12 դաս</li>
                                        <li>Շաբաթական 3 օր</li>
                                        <li>Դասաժամ 2 ժամ</li>
                                        <li>Մասնակիցները խմբում 2</li>
                                    </ul>
                                    <p class="reg-lessons-item-button-window">
                              <span class="btn-Register" data-toggle="modal" data-target="#signInModal"
                                    onclick="document.getElementById('dataLesson').value='PREMIUM_Offline'">
                                 <a href="#" class="mybtn-two">
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                        Գրանցվել
                                 </a>
                              </span>
                                    </p>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="reg-lessons-item">
                                    <h4 class="reg-lessons-item-name">PREMIUM</h4>
                                    <h1 class="reg-lessons-item-price">65․500 դրամ</h1>
                                    <h6 class="reg-lessons-item-terms">Դասընթացի պայմանները.</h6>
                                    <hr>
                                    <ul>
                                        <li>Ամիս 12 դաս</li>
                                        <li>Շաբաթական 3 օր</li>
                                        <li>Դասաժամ 2 ժամ</li>
                                        <li>Մասնակիցները խմբում 1</li>
                                    </ul>
                                    <p class="reg-lessons-item-button-window">

                              <span class="btn-Register" data-toggle="modal" data-target="#signInModal"
                                    onclick="document.getElementById('dataLesson').value='PREMIUM_Offline'">
                                 <a href="#" class="mybtn-two">
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                        Գրանցվել
                                 </a>
                              </span>
                                    </p>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
