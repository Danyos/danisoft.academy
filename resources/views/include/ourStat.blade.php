<section class="counter-area counter-bg pd overlay-black">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="counter-item text-center">
                    <i class="fa fa-book"></i>
                    <span class="count">500</span>
                    Դիմորդ
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="counter-item text-center">
                    <i class="fa fa-thumbs-o-up"></i>
                    <span class="count">300</span>
                    Ուսանող
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="counter-item text-center">
                    <i class="fa fa-users"></i>
                    <span class="count">50</span>
                    Վերապատրաստվել
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="counter-item text-center">
                    <i class="fa fa-code  "></i>
                    <span class="count">40</span>
                    Ընթացքի մեջ
                </div>
            </div>
        </div>
    </div>
</section>
