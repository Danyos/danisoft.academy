@extends("layouts.base")
@section('css')
    <style>
        .course-sidebar .course-widget {
            box-shadow: 0 30px 50px 0 rgb(51 51 51 / 8%);
            padding: 0px 40px 30px;
            border: 1px dashed #eee;
            border-top: 2px solid #385777;
        }
        .course-sidebar .course-widget ul li div {
            border-bottom: 1px solid #eee;
            padding: 10px 0;
        }
        .course-widget h4{
            display: inline-block;
            font-size: 25px;
        }
        .course-widget ul li{
            font-size: 17px;
            padding-left: 0;

        }.course-widget span{
            font-size: 17px;
            padding-left: 0;

        }
       .what-you-will-learn i.fa{
            margin-right: 25px;
        }
       #accordion1 li{
           list-style: disc;
       }
       .for-lesson-item+ h4{
           padding: 19px 0;
       }
    </style>
@endsection

@section('content')

<!-- Breadcrumb Area Start -->
<div class="breadcrumb-area breadcrumb-bg overlay-black pd-70">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>WEB 0 –ԻՑ</h2>
                <ul>
                    <li><a href="index-2.html">
                            ՕՆԼԱՅՆ ՕՖԼԱՅՆ ԴԱՍԸՆԹԱՑ ՈԼՈՐՏԻ ԱՌԱՋԱՏԱՐ ՄԻԱՋԶԳԱՅԻՆ ՍԵՐՏԵՖԻԿԱՑՎԱԾ ՄԱՍՆԱԳԵՏՆԵՐԻ ԿՈՂՄԻՑ
                        </a></li>

                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb Area End -->
<!-- Courses Area Start -->
<div class="courses-area pd" style="padding: 31px 0;">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="course-bg course-details">
                            <img src="{{asset('client/assets/images/lesson/web0.jpg')}}" style="height: 450px;width: 100%; object-fit: contain;" alt="">
                            <div class="course-title" >
                                <h2 style="color: white; font-size: 3em;">WEB 0 –ԻՑ</h2>
                            </div>
                        </div>
                        <div class="spacer-50"></div>
                        <div class="course-tab">
                            <div class="course-tab-item">
                                <ul class="course-tab-list-tem">
                                    <li><a   class="widget-title">Դասընթացի Նկարագրություն</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="tab-1">
                                        <p><b>Դասընթացը նախատեսված է սկսնակների համար:</b><br>
                                            Այն բավականին պրակտիկ և գործնական է` ՏՏ ոլորտի արդի պահանջներին համապատասխան։

                                        </p>

                                        <p>Վեբինարի ընթացքում կխոսվի ծրագրավորման դերը:<br> Դուք ձեռք կբերեք հմտություններ, խորհուրդներ որոնք կօգնեն Ձեր գիտելիքի զարգացման գործում: <br>Նաև, թե ինչպիսի սխալներից է հարկավոր խուսափել ծրագրավորման մեջ, ինչն է լավ աշխատում, իսկ ինչը՝ ոչ այնքան: </p>
                                        <div class="for-lesson-item" style="display: block!important;">
                                            <h5 style="font-weight: bold;color: black; padding: 17px 0; " >
                                                <sapn class="widget-title">Ի՞ՆՉ Է ՆԵՐԱՌՈՒՄ ԴԱՍԸՆԹԱՑԸ </sapn> </h5>
                                            <ul>
                                                <li>HTML5, CSS3</li>
                                                <li>JavaScript (ES6)</li>
                                                <li>React Js / Redux</li>
                                            </ul>

                                            <h6 style="font-weight: bold;color: black; padding: 17px 0;">

                                                <sapn class="widget-title">Հավելյալ նվեր բոնուս կուրս դասընթաց </sapn>
                                            </h6>
                                            <ul>
                                                <li>jQuery</li>
                                                <li> Bootstrap</li>
                                                <li> Material UI</li>
                                            </ul>
                                            <br>

                                            <ul>
                                                <li>Առաջատար և բարձր վարձատրվող մասնագիտություն</li>
                                                <li>Ամբողջովին պրակտիկ դասընթաց</li>
                                                <li>Դասընթացն ավարտելուց հետո դուք կկարողանաք ստեղծել ամբողջովին ֆունկցիոնալ կայք</li>
                                                <li>20% զեղչի հնարավորություն մեր մյուս դասընթացներից օգտվելու դեպքում</li>
                                                <li>10 % զեղչ մտերիմիդ համար</li>
                                                <li>Հավելյալ նվեր վեբինար ` smm-ը բիզնեսի համար</li>
                                                <li>Սերտիֆիկացում</li>
                                                <li>Հետադարձ կապի ապահովում`ուսանող-դասախոս</li>
                                                <li>Կունենաս աշխատանք DaniSoft-ում կամ համագարծակցող կազմակերպությունում</li>
                                            </ul>

                                   </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="what-you-will-learn">
                    <h3 class="widget-title">ԴԱՍԸՆԹԱՑԻ Մասին</h3>
                    <div class="course-sidebar">
                        <div class="course-widget course-details-info">
                            <ul>
                                <li>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span><i class="fa fa-money"></i>Արժեք - </span>
                                        <a href="{{route('client.price_list')}}" class="d-inline-block">
                                            <h4 class="course-price">
                                                <span class="price">
                                                    35․ 300 դրամ (Ամիս)
                                                </span>
                                            </h4>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span><i class="fa fa-user"></i>Խմբում - </span>
                                        5 հոգի
                                    </div>
                                </li>

                                <li>
                                    <div class="d-flex justify-content-between align-items-center">

                                        <span><i class="fa fa-file"></i>Տևողություն - </span>
                                        6 ամիս
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span><i class="fa fa-user"></i>Հաճախելիություն -</span>
                                       12 դաս
                                    </div>
                                </li><li>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span><i class="fa fa-clock-o"></i>Ժամ -</span>
                                        2 Ժամ
                                    </div>
                                </li>

                                <li>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span><i class="fa fa-calendar"></i>Մեկնարկ - </span>
                                        Սեպտեմբերի 1-ին
                                    </div>
                                </li>
                            </ul>
                            <div class="buy-btn">
                                <br><br>
                               <span class="btn-Register" data-toggle="modal" data-target="#signInModal"
                                     onclick="document.getElementById('dataLesson').value='PREMIUM_Offline'">
                                 <a href="#" class="mybtn-two">
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                        Գրանցվել
                                 </a>
                              </span>
                            </div>
                        </div>
                        <div class="widget widget-recent">
                            <h3 class="widget-title">Այլ դասընթացներ</h3>
                            <div class="recent-news">
                                <img src="{{asset('client/assets/img/recent-news-1.jpg')}}" alt="">
                                <p>Quis Nostrud Exercitatio Ullamco.</p>
                                <span>30 Aug 2016</span>
                            </div>
                            <div class="recent-news">
                                <img src="{{asset('client/assets/img/recent-news-1.jpg')}}" alt="">
                                <p>Quis Nostrud Exercitatio Ullamco.</p>
                                <span>30 Aug 2016</span>
                            </div>
                            <div class="recent-news">
                                <img src="{{asset('client/assets/img/recent-news-1.jpg')}}" alt="">
                                <p>Quis Nostrud Exercitatio Ullamco.</p>
                                <span>30 Aug 2016</span>
                            </div>
                        </div>
                    </div>


                    <div class="course-details">
                        <div class="spacer-50"></div>
                        <h3 class="widget-title">ՎՃԱՐՄԱՆ ԳՐԱՖԻԿԸ</h3>
                        <div class="accordion accordion-box-4">
                            <div class="panel-group" id="accordion1">


                                <p>
                                    ԱՄԲՈՂՋԱԿԱՆ ՎՃԱՐՈՒՄ ՝ մինչ դասընթացի մեկնարկը
                                    <br>
                                    ՄԱՍՆԱԿԻ ՎՃԱՐՈՒՄ 50 % վճարվում է մինչ դասընթացի մեկնարկը՝
                                </p>


                            </div>
                        </div>
                    </div>


                </div>
                <br>
            </div>
        </div>
    </div>
</div>
<!-- Courses Area End -->

@endsection
