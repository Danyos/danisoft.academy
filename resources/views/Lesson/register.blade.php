<html><head>
    <title>Գրանցվել {{$data_Lesson_type}} դասընթացին</title>
     <meta charset="utf-8">
    <meta name="viewport" content="height=device-height,
    width=device-width, initial-scale=1.0,
    minimum-scale=1.0, maximum-scale=1.0,
    user-scalable=no, target-densitydpi=device-dpi">
    <meta name="description" content="Ուսուցման գործընթացը հիմնված է գործնական խնդիրների և ուսուցչի հետ շփման վրա: Մենք միանման դասախոսություններ չունենք: Եվ գիտելիքն անմիջապես գործնականում ամրագրված է:">
    <meta id="dcngeagmmhegagicpcmpinaoklddcgon">
    <link href="{{asset('clent/images/DaniSoft.svg')}}" rel="icon">
    <link href="{{asset('clent/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('clent/css/register.css')}}" type="text/css">

</head>
<body>
<div class="bg-img">
    <div class="content">
        <header>
            <a href="http://danisoft.am">
                <img src="{{asset('clent/images/DaniSoft.png')}}" alt="վիկտորինա դասընթացին" height="150px">
            </a>
            <br>
            <span>Գրանցվել դասընթացներին</span></header>
        <div class="alert alert-success d-none" id="msg_div">
            <span id="res_message">Դուք գրանցվել եք դասընթացին <br>
            մեր մասնագետը կապ կհասատի Ձեզ հետ</span>
        </div>
        <form id="lessonRegister" method="post" action="{{route('client.lessonRegister')}}">
            @csrf
            <div class="field">
                <span class="fa fa-user"></span>
                <span class="text-danger"></span>
                <input type="text" placeholder="Անուն Ազգանուն" name="name" id="formGroupExampleInput">
                <br>
                <br>
            </div>
            <br>
            <div class="field">
                <span class="fa fa-user"></span>
                <span class="text-danger"></span>
                <input type="number" required="" placeholder="Տարիք" name="age">

            </div>
            <br>
            <div class="field">
                <span class="fa fa-user"></span>
                <span class="text-danger"></span>
                <input type="tel" name="tel" placeholder="Հեռախոսահամար">

            </div>
            <br>
            <div class="field">
                <span class="fa fa-user"></span>
                <span class="text-danger"></span>
                <input type="email" placeholder="Փոստ" name="email" id="email">


                <input type="hidden" value="{{$data_Lesson_type}}" name="slug">
            </div>
            <br>

            <div class="pass">
                <br>
            </div>
            <div class="field">
                <input type="submit" value="Հաստատել" id="send_forms_data">
            </div>
        </form>
        <br>
        <div class="signup">Խնդրում ենք հաստատումից առաջ ստուգել Ձեր կողմից լրացված տվյալները

        </div>
    </div>
</div>

<script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-129525178-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-129525178-1');
</script>

<script src="{{asset('clent/js/jquery/jqueryAjax.min.js')}}"></script>
<script src="{{asset('clent/bootstrap/js/pupoper.js')}}"></script>


<script src="{{asset('clent/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script>

    if ($("#lessonRegister").length > 0) {

        $("#lessonRegister").validate({
            rules: {
            name: {
                required: true,
                    digits: false,
                    maxlength: 50
                },
            tel: {
                required: true,
                    digits: true,
                    minlength: 9,
                    maxlength: 14,
                },

            email: {
                required: true,
                    maxlength: 50,
                    email: true,
                },
            age: {
                required: true,
                    digits: true,
                    minlength: 2,
                    maxlength: 2,
                },
        },
            messages: {
            name: {
                required: "Խնդրում ենք լրացնել Անվանման դաշտը",
                    maxlength: "Շատ երկար է դաշտը"
                },
            tel: {
                required: "Խնդրում ենք լրացնել հեռախոսահամարի դաշտը",
                    digits: "Միայն թիվ",
                    minlength: "Ձեր հեռախոսահամրը չի համապատսխանում",
                    maxlength: "Գրեք ձեր ճիշտ հեռախոսահամրը",
                },

            email: {
                required: "Խնդրում ենք լրացնել փոստը դաշտը",
                email: "Գրեք ձեր փոստը",
                },
            age: {
                required: "Խնդրում ենք լրացնել տարիքի դաշտը",
                    digits: "Միայն թիվ",
                    minlength: "Ձեր տարիքը չի համապատսխանում",
                    maxlength: "Գրեք ձեր ճիշտ տարիքը"
                },
        },
            submitHandler: function (form) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    }
                });
                $('#send_forms_data').html('Բեռնում..');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '{{route('client.lessonRegister')}}',
                    type: "get",
                    data: $('#lessonRegister').serialize(),
                    success: function (response) {

                $('#send_form').html('Հաստատված');

                $('#res_message').show();

                $('#res_message').html(response.msg);

                $('#msg_div').removeClass('d-none');


                document.getElementById("lessonRegister").reset();

                setTimeout(function () {

                    $('#res_message').hide();
                    window.location.href="{{route('client.index')}}"
                    $('#msg_div').hide();
                    $('#send_forms_data').html('Ուղարկված');
                }, 4000);
            }
                });
            }
        })
    }

</script>

</body>
</html>
