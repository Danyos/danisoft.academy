@extends("layouts.base")
@section('content')
    <div class="breadcrumb-area breadcrumb-bg overlay-black pd-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Դասընթացներ</h2>
                    <ul>
                        <li><a href="{{route('client.index')}}">Գլխավոր</a></li>
                        <span class="saparator"><i class="fa fa-long-arrow-right"></i></span>
                        <li>Դասընթացներ</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End -->
    <!-- Courses Area Start -->
    <div class="courses-area pd-top-100">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="section-title">Frontend դասընթաց</h2>
                </div>
                <div class="col-sm-6">
                    <div class="search-widget">
                        <form action="https://themeunique.com/Studyplus/blog.html" method="post">
                            <input placeholder="Search Your Courses" type="search">
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="courses-box-item">
                        <div class="course-bg course-bg-1">
                            <i class="like fa fa-heart"></i>
                            <div class="course-title">
                                <h4>ՎԵԲ ԾՐԱԳՐԱՎՈՐՈՒՄԸ 0-ԻՑ</h4>
                                <ul class="course-date">

                                    <li><i class="fa fa-clock-o"></i>6 ամիս</li>
                                </ul>
                            </div>
                            <div class="course-teach-img">
                                <img src="assets/images/lesson/type/basic.png" style="width: 70px;height: 70px;" alt="">

                            </div>
                        </div>
                        <div class="course-desc">
                            <h4>basic course</h4>
                            <p>Պատրա՞ստ ես սկսելու քո ծրագրավորման ուղին, դառնալ պահանջված ծրագրավորող ,ապա մեր դասընթացը հենց քեզ համար է</p>
                            <div class="course-rating">
                                <div class="btnreg">
                                    <div >
                                        <a href="#" class="mybtn-two">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            Գրանցվել
                                        </a>
                                    </div>
                                    <div></div>
                                    <div >
                                        <a href="{{route('client.lessonMore','web0')}}" class="mybtn">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            Կարդալ Ավելին
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="courses-box-item">
                        <div class="course-bg course-bg-2">
                            <i class="like fa fa-heart"></i>
                            <div class="course-title">
                                <h4>Javascript դասընթաց</h4>
                                <ul class="course-date">

                                    <li><i class="fa fa-clock-o"></i>3 ամիս</li>
                                </ul>
                            </div>
                            <div class="course-teach-img">
                                <img src="assets/images/lesson/type/basic.png" style="width: 70px;height: 70px;" alt="">

                            </div>
                        </div>
                        <div class="course-desc">
                            <h4>basic course</h4>
                            <p>Դասընթացը սկսելու համար ձեզ հարկավոր չէ JavaScript-ի նախնական գիտելիքներ:</p>
                            <div class="course-rating">
                                <div class="btnreg">
                                    <div >
                                        <a href="#" class="mybtn-two">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            Գրանցվել
                                        </a>
                                    </div>
                                    <div></div>
                                    <div >
                                        <a href="#" class="mybtn">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            Կարդալ Ավելին
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="courses-box-item">
                        <div class="course-bg course-bg-3">
                            <i class="like fa fa-heart"></i>
                            <div class="course-title">
                                <h4>React js դասընթաց</h4>
                                <ul class="course-date">

                                    <li><i class="fa fa-clock-o"></i>2 ամիս</li>
                                </ul>
                            </div>
                            <div class="course-teach-img">
                                <img src="assets/images/lesson/type/basic.png" style="width: 70px;height: 70px;" alt="">
                            </div>
                        </div>
                        <div class="course-desc">
                            <h4>basic course</h4>
                            <p>React js-ը պարզապես սիրված չէ. Այլ ապագա է կարող են մասնակցել այն անձիք ովքերն ունեն Javascript բավարար գիտելիք</p>
                            <div class="course-rating">
                                <div class="btnreg">
                                    <div >
                                        <a href="#" class="mybtn-two">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            Գրանցվել
                                        </a>
                                    </div>
                                    <div></div>
                                    <div >
                                        <a href="#" class="mybtn">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            Կարդալ Ավելին
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <!-- Courses Area End -->
    <div class="populer-courses  pd">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title">Backend դասընթաց</h2>
                </div>
            </div>
            <div class="row">


                <div class="col-md-4 col-sm-6">
                    <div class="courses-box-item">
                        <div class="course-bg course-bg-4">
                            <i class="like fa fa-heart"></i>
                            <div class="course-title">
                                <h4>PHP դասընթաց</h4>
                                <ul class="course-date">

                                    <li><i class="fa fa-clock-o"></i>3 ամիս</li>
                                </ul>
                            </div>
                            <div class="course-teach-img">
                                <img src="assets/images/lesson/type/basic.png" style="width: 70px;height: 70px;" alt="">

                            </div>
                        </div>
                        <div class="course-desc">
                            <h4>basic course</h4>
                            <p>Բացի այն, որ PHP-ը շատ հեշտ է սովորել։ Դասընթացը սկսելու համար ձեզ հարկավոր չէ PHP-ի նախնական գիտելիքներ:</p>
                            <div class="course-rating">
                                <div class="btnreg">
                                    <div >
                                        <a href="#" class="mybtn-two">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            Գրանցվել
                                        </a>
                                    </div>
                                    <div></div>
                                    <div >
                                        <a href="#" class="mybtn">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            Կարդալ Ավելին
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
    <!-- Populer Area Start -->
    <div class="populer-courses  pd">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title">Երեխաների դասընթաց</h2>
                </div>
            </div>
            <div class="row">

                <div class="col-md-4 col-sm-6">
                    <div class="courses-box-item">
                        <div class="course-bg course-bg-5">
                            <i class="like fa fa-heart"></i>
                            <div class="course-title">
                                <h4>«Ապագա Փոքրիկ ծրագրավորող»</h4>
                                <ul class="course-date">

                                    <li><i class="fa fa-clock-o"></i>3 ամիս</li>
                                </ul>
                            </div>
                            <div class="course-teach-img">
                                <img src="assets/images/lesson/type/basic.png" style="width: 70px;height: 70px;" alt="">

                            </div>
                        </div>
                        <div class="course-desc">
                            <h4>basic course</h4>
                            <p>«Ապագա Փոքրիկ ծրագրավորող» դասընթացը՝ 𝟭𝟯-𝟭𝟲տ երեխաների համար</p>
                            <div class="course-rating">
                                <div class="btnreg">
                                    <div >
                                        <a href="#" class="mybtn-two">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            Գրանցվել
                                        </a>
                                    </div>
                                    <div></div>
                                    <div >
                                        <a href="#" class="mybtn">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            Կարդալ Ավելին
                                        </a>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <!-- Populer Area End -->

@endsection
