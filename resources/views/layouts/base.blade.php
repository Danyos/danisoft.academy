<!DOCTYPE html>
<html lang="am">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>DaniSoft Academy</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('client/assets/css/bootstrap.min.css')}}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{asset('client/assets/css/font-awesome.min.css')}}">
    <!-- Owl Carousel CSS-->
    <link rel="stylesheet" href="{{asset('client/assets/css/owl.carousel.css')}}">
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{asset('client/assets/css/nice-select.css')}}">
    <!-- Slicknav CSS -->
    <link rel="stylesheet" href="{{asset('client/assets/css/slicknav.min.css')}}">
    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="{{asset('client/assets/css/magnific-popup.css')}}">
    <!-- Progress CSS -->

    <link rel="stylesheet" href="{{asset('client/assets/css/custom-progress.css')}}">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{asset('client/assets/css/style.css')}}">
    @yield('css')
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('client/assets/css/responsive.css')}}">
</head>
<body>
<div class="studyplus-preloader-wrapper">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<header class="header-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="logo">
                    <h2><a href="{{route('client.index')}}"><img src="{{asset('client/assets/images/DaniSoft.png')}}" style="width: 136px;
                  height: 69px;object-fit: cover;" alt="DaniSoft Logo "></a></h2>
                </div>
            </div>
            <div class="col-sm-8 text-right">
                <a href="tel:098657545" class="studyplus-contact-box">
                    <i class="fa fa-phone"></i>
                    <p>+374 (43) 65-75-45</p>
                    <p>+374 (98) 65-75-45</p>
                </a>
                <a href="email:info@danisoft.am" class="studyplus-contact-box">
                    <i class="fa fa-envelope"></i>
                    <p>info@danisoft.am</p>
                    <p>DaniSoftAcademy@gmail.com</p>
                </a>
                <a href="{{route('client.contact')}}" class="studyplus-contact-box">
                    <i class="fa fa-map "></i>
                    <p>Քաղաք Երևան</p>
                    <p>Գյուլբեկյան 23 </p>
                </a>
            </div>
        </div>
        <div class="responsive-menu"></div>
    </div>
</header>
<nav class="mainmenu-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-md-12 col-sm-12">
                <div class="mainmenu">
                    <ul>
                        <li class="{{request()->is('/')  ? 'active' : '' }}">
                            <a href="{{route('client.index')}}">Գլխավոր</a>
                        </li>
                        <li class="{{request()->is('about')  ? 'active' : '' }}">
                            <a href="{{route('client.about')}}">Մեր Մասին</a></li>
                        <li  class="{{request()->is('lesson')  ? 'active' : '' }}">
                            <a href="{{route('client.lesson_danisoft')}}">Դասընթացներ</a>
                        </li>
                        <li  class="{{request()->is('new_start')  ? 'active' : '' }}">
                            <a href="{{route('client.new_start')}}">Մեկնարկող դասընթաց </a>
                        </li>
                        <li class="{{request()->is('reviews')  ? 'active' : '' }}">
                            <a href="{{route('client.reviews')}}">Կարծիքներ</a>
                        </li>
                        <li class="{{request()->is('blog')  ? 'active' : '' }}">
                            <a href="{{route('client.blog')}}">Բլոգ</a>
                        </li>
                        <li class="{{request()->is('contact')  ? 'active' : '' }}">
                            <a href="{{route('client.contact')}}">Կապ մեզ հետ</a>
                        </li>
                    </ul>
                </div>
            </div>
{{--            <div class="col-sm-2">--}}

{{--                <a href="contact.html">Կապ մեզ հետ</a>--}}
{{--            </div>--}}
        </div>
    </div>
</nav>

@yield('content')
<footer class="site-footer">

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <p>All Rights Reserved © Designed By DaniSoft </p>
                </div>
                <div class="col-sm-2 text-center">
                    <div class="scroll-top">
                        <i class="fa fa-angle-double-up"></i>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="social-link text-right">

                                <a href="https://fb.com/danisoft.am" target="_blank">
                                    <i class="fa fa-facebook-f"></i>
                                </a>

                                <a  href="https://linkedin.com//company/danisoft" target="_blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>

                                <a href="https://t.me/DanisoftAM" target="_blank">
                                    <i class="fa fa-telegram"></i>
                                </a>

                                <a href="https://instagram.com/danisoft.am"  target="_blank">
                                    <i class="fa fa-instagram"></i>
                                </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
@include('include.registerLessonModal')
<script type="text/javascript" src="{{asset('client/assets/js/jquery.min.js')}}"></script>
<!-- Bootstrap JS -->
<script type="text/javascript" src="{{asset('client/assets/js/bootstrap.min.js')}}"></script>
<!-- Owl  Carousel JS -->
<script type="text/javascript" src="{{asset('client/assets/js/owl.carousel.min.js')}}"></script>
<!-- Nice Select JS -->
<script type="text/javascript" src="{{asset('client/assets/js/jquery.nice-select.min.js')}}"></script>
<!-- Waypoints JS -->
<script type="text/javascript" src="{{asset('client/assets/js/waypoints.min.js')}}"></script>
<!-- Counterup JS -->
<script type="text/javascript" src="{{asset('client/assets/js/jquery.counterup.js')}}"></script>
<!-- Countdown Timer JS -->
<script type="text/javascript" src="{{asset('client/assets/js/countdown-timer.min.js')}}"></script>
<!-- Slicknav Timer JS -->
<script type="text/javascript" src="{{asset('client/assets/js/jquery.slicknav.min.js')}}"></script>
<!-- Magnific Popup JS -->
<script type="text/javascript" src="{{asset('client/assets/js/jquery.magnific-popup.min.js')}}"></script>
<!-- Active JS -->
<script type="text/javascript" src="{{asset('client/assets/js/active.js')}}"></script>
@yield('js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script>
    $('#msg_div').hide()
    $('#msg_Action_Register').hide()
    if ($("#lessonRegister").length > 0) {

        $("#lessonRegister").validate({
            rules: {
                name: {
                    required: true,
                    digits: false,
                    maxlength: 50
                },
                tel: {
                    required: true,
                    digits: true,
                    minlength: 9,
                    maxlength: 14,
                },

                email: {
                    required: true,
                    maxlength: 50,
                    email: true,
                },
                age: {
                    required: true,
                    digits: true,
                    minlength: 2,
                    maxlength: 2,
                },
            },
            messages: {
                name: {
                    required: "Լրացնել դաշտը",
                    maxlength: "Շատ երկար է դաշտը"
                },
                tel: {
                    required: "Լրացնել դաշտը",
                    digits: "Միայն թիվ",
                    minlength: "Ձեր հեռախոսահամրը չի համապատսխանում",
                    maxlength: "Գրեք ձեր ճիշտ հեռախոսահամրը",
                },

                email: {
                    required: "Լրացնել դաշտը",
                    email: "Գրեք ձեր փոստը",
                },
                age: {
                    required: "Լրացնել դաշտը",
                    digits: "Միայն թիվ",
                    minlength: "Ձեր տարիքը չի համապատսխանում",
                    maxlength: "Գրեք ձեր ճիշտ տարիքը"
                },
            },
            submitHandler: function (form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    }
                });
                $('#send_forms_data').html('Բեռնում..');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '{{route('client.lessonRegister')}}',
                    type: "get",
                    data: $('#lessonRegister').serialize(),
                    success: function (response) {


                        $('#send_form').html('Հաստատված');

                        $('#msg_div').show();
                        $('.d-none-js').show();

                        $('#res_message').html(response.msg);




                        document.getElementById("lessonRegister").reset();

                        setTimeout(function () {

                            $('#msg_div').hide();
                            $('.d-none-js').hide();


                            $('#send_form').html('Հաստատել');
                            $('#closeModal').trigger('click');
                        }, 4000);
                    }
                });
            }
        })
    }
    if ($("#lessonActionRegister").length > 0) {

        $("#lessonActionRegister").validate({
            rules: {
                name: {
                    required: true,
                    digits: false,
                    maxlength: 50
                },
                tel: {
                    required: true,
                    digits: true,
                    minlength: 9,
                    maxlength: 14,
                },

                email: {
                    required: true,
                    maxlength: 50,
                    email: true,
                },
                age: {
                    required: true,
                    digits: true,
                    minlength: 2,
                    maxlength: 2,
                },
            },
            messages: {
                name: {
                    required: "Խնդրում ենք լրացնել Անվանման դաշտը",
                    maxlength: "Շատ երկար է դաշտը"
                },
                tel: {
                    required: "Խնդրում ենք լրացնել հեռախոսահամարի դաշտը",
                    digits: "Միայն թիվ",
                    minlength: "Ձեր հեռախոսահամրը չի համապատսխանում",
                    maxlength: "Գրեք ձեր ճիշտ հեռախոսահամրը",
                },

                email: {
                    required: "Խնդրում ենք լրացնել փոստը դաշտը",
                    email: "Գրեք ձեր փոստը",
                },
                age: {
                    required: "Խնդրում ենք լրացնել տարիքի դաշտը",
                    digits: "Միայն թիվ",
                    minlength: "Ձեր տարիքը չի համապատսխանում",
                    maxlength: "Գրեք ձեր ճիշտ տարիքը"
                },
            },
            submitHandler: function (form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    }
                });
                $('#send_form_action').html('Բեռնում..');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '{{route('client.lessonRegister')}}',
                    type: "get",
                    data: $('#lessonActionRegister').serialize(),
                    success: function (response) {

                        $('#send_form_action').html('Հաստատված');

                        $('#msg_Action_Register').show();

                        $('#msg_Action_Register').html(response.msg);

                        $('#msg_div').removeClass('d-none');


                        document.getElementById("lessonActionRegister").reset();


                        setTimeout(function () {

                            $('#msg_Action_Register').hide();

                            $('#msg_Action_Register').hide();
                            $('#send_form_action').html('Ուղարկված');
                        }, 4000);
                    }
                });
            }
        })
    }

</script>
</body>
</html>

