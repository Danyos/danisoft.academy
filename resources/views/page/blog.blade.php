@extends("layouts.base")
@section('menu')

@endsection
@section('content')
    <div class="breadcrumb-area breadcrumb-bg overlay-black pd-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Բլոգ</h2>
                    <ul>
                        <li><a href="{{route('client.index')}}">Գլխավոր</a></li>
                        <span class="saparator"><i class="fa fa-long-arrow-right"></i></span>
                        <li>Բլոգ</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area pd-top-100 pd-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="blog-item-box">
                        <div class="blog-img">
                            <img src="assets/img/blog-img-1.jpg" alt="">
                            <div class="blog-date">15 <span>Feb</span></div>
                        </div>
                        <div class="text-box">
                            <h4>Those Other College Expenses You</h4>
                            <p>Nam liber tempor cum soluta nobis eleifend ocongue nihil imperdiet doming  <a href="#" class="read-more-btn">Contiune Reading...</a> </p>
                            <ul>
                                <li>By Admin</li>
                                <li><i class="fa fa-heart"></i> 25</li>
                                <li><i class="fa fa-eye"></i> 200</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="blog-item-box">
                        <div class="blog-img">
                            <img src="assets/img/blog-img-2.jpg" alt="">
                            <div class="blog-date">15 <span>Feb</span></div>
                        </div>
                        <div class="text-box">
                            <h4>Those Other College Expenses You</h4>
                            <p>Nam liber tempor cum soluta nobis eleifend ocongue nihil imperdiet doming  <a href="#" class="read-more-btn">Contiune Reading...</a> </p>
                            <ul>
                                <li>By Admin</li>
                                <li><i class="fa fa-heart"></i> 25</li>
                                <li><i class="fa fa-eye"></i> 200</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="blog-item-box">
                        <div class="blog-img">
                            <img src="assets/img/blog-img-3.jpg" alt="">
                            <div class="blog-date">15 <span>Feb</span></div>
                        </div>
                        <div class="text-box">
                            <h4>Those Other College Expenses You</h4>
                            <p>Nam liber tempor cum soluta nobis eleifend ocongue nihil imperdiet doming  <a href="#" class="read-more-btn">Contiune Reading...</a> </p>
                            <ul>
                                <li>By Admin</li>
                                <li><i class="fa fa-heart"></i> 25</li>
                                <li><i class="fa fa-eye"></i> 200</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="blog-item-box">
                        <div class="blog-img">
                            <img src="assets/img/blog-img-1.jpg" alt="">
                            <div class="blog-date">15 <span>Feb</span></div>
                        </div>
                        <div class="text-box">
                            <h4>Those Other College Expenses You</h4>
                            <p>Nam liber tempor cum soluta nobis eleifend ocongue nihil imperdiet doming  <a href="#" class="read-more-btn">Contiune Reading...</a> </p>
                            <ul>
                                <li>By Admin</li>
                                <li><i class="fa fa-heart"></i> 25</li>
                                <li><i class="fa fa-eye"></i> 200</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="blog-item-box">
                        <div class="blog-img">
                            <img src="assets/img/blog-img-2.jpg" alt="">
                            <div class="blog-date">15 <span>Feb</span></div>
                        </div>
                        <div class="text-box">
                            <h4>Those Other College Expenses You</h4>
                            <p>Nam liber tempor cum soluta nobis eleifend ocongue nihil imperdiet doming  <a href="#" class="read-more-btn">Contiune Reading...</a> </p>
                            <ul>
                                <li>By Admin</li>
                                <li><i class="fa fa-heart"></i> 25</li>
                                <li><i class="fa fa-eye"></i> 200</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="blog-item-box">
                        <div class="blog-img">
                            <img src="assets/img/blog-img-3.jpg" alt="">
                            <div class="blog-date">15 <span>Feb</span></div>
                        </div>
                        <div class="text-box">
                            <h4>Those Other College Expenses You</h4>
                            <p>Nam liber tempor cum soluta nobis eleifend ocongue nihil imperdiet doming  <a href="#" class="read-more-btn">Contiune Reading...</a> </p>
                            <ul>
                                <li>By Admin</li>
                                <li><i class="fa fa-heart"></i> 25</li>
                                <li><i class="fa fa-eye"></i> 200</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
