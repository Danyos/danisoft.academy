@extends("layouts.base")
@section('menu')

@endsection
@section('content')
    <div class="breadcrumb-area breadcrumb-bg overlay-black pd-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Կարծիքներ</h2>
                    <ul>
                        <li><a href="{{route('client.index')}}">Գլխավոր</a></li>
                        <span class="saparator"><i class="fa fa-long-arrow-right"></i></span>
                        <li>Կարծիքներ</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @include('include.reviews')

@endsection
