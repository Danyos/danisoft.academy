@extends("layouts.base")
@section('menu')

@endsection
@section('content')
    <div class="breadcrumb-area breadcrumb-bg overlay-black pd-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Կապ Մեզ հետ</h2>
                    <ul>
                        <li><a href="{{route('client.index')}}">Գլխավոր</a></li>
                        <span class="saparator"><i class="fa fa-long-arrow-right"></i></span>
                        <li>Կապ</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End -->
    <!-- Contact Area Start -->
    <div class="contact-us-area pd">
        <div class="container">

            <div class="spacer-50"></div>
            <div class="row">
                <div class="col-md-3 col-sm-5">
                    <div class="contact-number">
                        <p><i class="fa fa-phone"></i>+1234 (0123)  789</p>
                        <p><i class="fa fa-map-marker"></i>Amar bari , Tomar bari , U S A</p>
                        <p><i class="fa fa-envelope"></i>youremail@gmail.com</p>
                    </div>
                </div>
                <div class="col-md-9 col-sm-7">
                    <div class="contact-info-text">
                        <h3>contact info</h3>
                        <p>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectlegere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. </p>
                    </div>
                </div>
            </div>
            <div class="spacer-50"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="contact-form">
                        <h4>Write a message</h4>
                        <div class="search-box" id="registerLesson">
                            <form action="#">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p><input class="form-control" type="text" placeholder="Name"></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p><input class="form-control" type="text" placeholder="Email"></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><textarea class="form-control" name="" placeholder="Message"></textarea></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><input type="submit" value="Send Message"></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
