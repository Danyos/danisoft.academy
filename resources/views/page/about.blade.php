@extends("layouts.base")
@section('content')
    <div class="breadcrumb-area breadcrumb-bg overlay-black pd-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Մեր Մասին</h2>
                    <ul>
                        <li><a href="{{route('client.index')}}">Գլխավոր</a></li>
                        <span class="saparator"><i class="fa fa-long-arrow-right"></i></span>
                        <li>Մեր Մասին</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<div class="about-our-schoor-area pd">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">welcome to our school</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="about-box-item">
                    <img src="{{asset('client/assets/images/web/dasntacner.png')}}" alt="">
                    <p>Commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate vel molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusdignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla fa liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerfacer possim assum. Typi non habent claritatem insitam; est usus.</p>
                    <p>Commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate vel molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusdignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla .</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="about-box-item">
                    <img src="{{asset('client/assets/images/about/danisoft_birtday.jpg')}}" alt="">
                    <p>Commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate vel molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusdignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla fa liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerfacer possim assum. Typi non habent claritatem insitam; est usus.</p>
                    <div class="author-signature">
                        <img src="{{asset('client/assets/images/about/student_of_danisoft.jpg')}}" alt="">
                        <h4>Michel jhon <span>CEO & Founder</span></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About School Area End -->
<!-- Counter Area Start -->
@include('include.ourStat')
<!-- Counter Area End -->
<!-- Lecturera Area Start -->
@include('include.teacher')
<!-- Lecturera Area End -->
<!-- Testimonial Area Start -->

@include('include.reviews')
@endsection
