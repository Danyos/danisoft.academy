<div class="side-menu-area">

    <div class="classy-nav-container breakpoint-off">
        <!-- Classy Menu -->
        <nav class=" justify-content-between" id="classyNav">

            <!-- Navbar Toggler -->
            <div class="classy-navbar-toggler">
                <span class="navbarToggler"><span></span><span></span><span></span></span>
            </div>

            <!-- Menu -->
            <div class="classy-menu">
                <!-- close btn -->
                <div class="classycloseIcon">
                    <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                </div>

                <!-- Nav Start -->
                <div class="classynav">
                    <ul>

                        <li><a href="{{ route("admin.home") }}"><i class="icon_drive"></i> ГЛАВНАЯ</a></li>
                        @can('user_management_access')
                        <li><a href="#"><i class="icon_briefcase"></i> User</a>
                            <ul class="dropdown">
                                <li>  <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                        <i class="fas fa-unlock-alt nav-icon">

                                        </i>
                                        Разрешения
                                    </a></li>
                                <li>      <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                        <i class="fas fa-briefcase nav-icon">

                                        </i>
                                        Позиция
                                    </a>
                                </li>
                                <li>    <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                        <i class="fas fa-user nav-icon">

                                        </i>
                                        Пользователь
                                    </a></li>
                            </ul>
                        </li>
                        @endcan
   <?php $register=App\Models\Lesson\RegisterLessonModel::where('status','inactive')->get();
    $Ints= App\Models\CompetitionModel::find(1);
   ?>

                        @can('slider_accses')
                        <li> <a href="{{route('admin.slider.index')}}"><i class="icon_images"></i>слайдер</a></li>
                        <li> <a href="{{route('admin.okeyGoogle')}}"><i class="icon_images"></i>Անջատել նկարը {{$Ints->type}}</a></li>
                        @endcan
                        @can('slider_accses')
                        <li> <a href="{{route('admin.campetition.index')}}"><i class="icon_images"></i>Մրցույթ</a></li>
                        @endcan
                        @can('slider_accses')
                        <li> <a href="{{route('admin.campetition.show',"Մրցույթ")}}"><i class="icon_images"></i>Սկսել Մրցույթ</a></li>
                        @endcan
                      @can('service_access')
                        <li> <a href="{{route('admin.service.index')}}"><i class="icon_genius"></i>НАШИ УСЛУГИ</a></li>
                        @endcan
                        @can('contact_accses')
                        <li> <a href="{{route('admin.contact.index')}}"><i class="fa fa-envelope-o"></i>Сообщения</a></li>
                        @endcan
                        @can('news_access')
                        <li> <a href="{{route('admin.package.Lesson')}}"  class="nav-link {{ request()->is('admin/packageLesson') || request()->is('admin/blog/*') ? 'active' : '' }}"><i class="icon_pencil-edit"></i>Register Lesson  <span style="color:red;margin-left:15px"> {{$register->count()}}</span></a></li>
                        @endcan
                        @can('news_access')
                        <li> <a href="{{route('admin.package.Service')}}"  class="nav-link {{ request()->is('admin/packageService') || request()->is('admin/packageService/*') ? 'active' : '' }}"><i class="icon_pencil-edit"></i>Register Service</a></li>
                        @endcan
                        @can('news_access')
                        <li> <a href="{{route('admin.blog.index')}}"  class="nav-link {{ request()->is('admin/blog') || request()->is('admin/blog/*') ? 'active' : '' }}"><i class="icon_pencil-edit"></i>БЛОГ</a></li>
                        @endcan

                            @can('about_accses')
                        <li><a href="#"><i class="icon_desktop"></i>  О НАС</a>
                            <ul class="dropdown">
                                <li><a href="{{ route("admin.about.index") }}">- Базовый</a></li>
                                <li><a href="{{ route("admin.aboutinform.index") }}">- Информация</a></li>
                            </ul>
                        </li>
                        @endcan


                        @can('about_accses')
                        <li><a href="#"><i class="icon_desktop"></i>Դասընթացներ </a>
                            <ul class="dropdown">
                                <li><a href="{{ route("admin.trainers.index") }}">- Դասավանդող</a></li>
                                <li><a href="{{ route("admin.lessonPrice.index") }}">- Գումարային</a></li>
                                <li><a href="{{ route("admin.lesson.index") }}">- Դասընթացներ</a></li>
                                <li><a href="{{ route("admin.special.index") }}">- Հատուկ Դասընթացներ</a></li>
                            </ul>
                        </li>
                        @endcan


                    </ul>
                </div>
                <!-- Nav End -->
            </div>
        </nav>
    </div>
</div>
