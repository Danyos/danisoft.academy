@extends('admin.layouts.admin')
@section('css')
    <link href="{{asset('admin_style/assets/css/apps/notes.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin_style/assets/css/forms/theme-checkbox-radio.css')}}" rel="stylesheet" type="text/css"/>
    <style>
        .validation-text {
            color: red;
        }
    </style>
@endsection
@section('js')
{{--        <script src="{{asset('admin_style/assets/js/ie11fix/fn.fix-padStart.js')}}"></script>--}}
{{--    <script src="{{asset('admin_style/assets/js/apps/notes.js')}}"></script>--}}
    <script src="{{asset('admin_style/js/fetch.js')}}"></script>

    <script src="{{asset('admin_style/assets/js/apps/notes.js')}}"></script>
    <script>


        function trashnotes(ids){
            let con=confirm("Press a button!");
            if (con){
                var token = $("input[name='_token']").val();
                var url = "{{url('admin/registerLessonDelete/')}}/"+ids
                var method='post'
                fetchsend(token, url, method,ids,'datatrash')
            }

        }


    </script>
@endsection
@section('content')

    <?php
    $section_menu = "Հուշատետր";
    $section_menu_url = "admin.notes.index";


    ?>

    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="row app-notes layout-top-spacing" id="cancel-row">
                <div class="col-lg-12">
                    <div class="app-hamburger-container">
                        <div class="hamburger">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-menu chat-menu d-xl-none">
                                <line x1="3" y1="12" x2="21" y2="12"></line>
                                <line x1="3" y1="6" x2="21" y2="6"></line>
                                <line x1="3" y1="18" x2="21" y2="18"></line>
                            </svg>
                        </div>

                    </div>

                    <div class="app-container">

                        <div class="app-note-container">

                            <div class="app-note-overlay"></div>

                            <div class="tab-title">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-12 text-center">
                                        <a id="btn-add-notes" class="btn btn-primary" href="javascript:void(0);">Ավելացնել</a>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-12 mt-5">
                                        <h2 align="center">{{$lessonRegcount}}</h2>
                                        <ul class="nav nav-pills d-block" id="pills-tab3" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link list-actions {{ request()->is('admin/registerLesson/all') ? 'active' : '' }}" id="all-notes"  href="{{route('admin.registerLesson.show','all')}}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-edit">
                                                        <path
                                                            d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                                        <path
                                                            d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                                    </svg>
                                                    Վերադարձ</a>
                                            </li>
                                        </ul>

                                        <hr/>

                                    </div>
                                </div>
                            </div>


                            <div id="ct" class="note-container note-grid">
                                @foreach($lessonReg as $k=>$comments)

                                    <div class="note-item all-notes note-{{$note->status}}" id="trash{{$note->id}}">
                                        <div class="note-inner-content">
                                            <div class="note-content">
                                                <p class="note-title"
                                                   data-noteTitle="Meeting with Kelly">{{$note->name}}</p>
                                                <p class="meta-time">{{$note->email}}</p><br>
                                                <p class="meta-time">{{$note->tel}}</p>
                                                <br>
                                                <big>Խոսալու արդյունքները </big>
                                                <br>
                                                <br>
                                                <p class="meta-time">{{$comments->created_at}}</p><br>
                                                <div class="note-description-content">
                                                    <p class="note-description"
                                                       data-noteDescription="{{$comments->comments}}">{{$comments->comments}}</p>
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                @endforeach
                            </div>

                        </div>

                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="notesMailModal" tabindex="-1" role="dialog"
                         aria-labelledby="notesMailModalTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-x close" data-dismiss="modal">
                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                    </svg>
                                    <div class="notes-box">
                                        <div class="notes-content">
                                            <form action="{{route('admin.lessonRegComments.info')}}" method="post">
                                                @csrf
                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="d-flex note-description">
                                                          <textarea id="n-description" class="form-control"
                                                                    placeholder="Ինչ ես խոսել" name="comments"></textarea>

                                                        </div>
                                                        <input type="hidden" value="{{$id}}" name="reg_id">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="modal-footer">
                                                    <button  type="submit"  class="btn">Add
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="footer-wrapper">
            <div class="footer-section f-section-1">
                <p class="">Copyright © 2020 <a target="_blank" href="https://designreset.com/">DesignReset</a>, All
                    rights reserved.</p>
            </div>
            <div class="footer-section f-section-2">
                <p class="">Coded with
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-heart">
                        <path
                            d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
                    </svg>
                </p>
            </div>
        </div>
    </div>
    <!--  END CONTENT AREA  -->

@endsection
