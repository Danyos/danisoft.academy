@extends('admin.layouts.admin')
@section('content')

    <?php


    $settings = false;
    $section_menu = "Նախորդ էջ";
    $section_menu_url = "admin.LessonTimeList.index";
    $section_menu_section_create="Վճարումներ";

    ?>

    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped mb-4">
                    <thead>
                    <tr>
                        <th>Համար</th>
                        <th>Անուն</th>
                        <th>Ամսվա սկիզբ</th>
                        @if($data=="on")
                            <th>Ամսվա վերջ</th>
                        @else
                            <th>օր</th>
                        @endif
                        <th class="text-center">Գումար</th>
                        @if($data=="on")
                        <th class="text-center">Տեսակը</th>

                            <th class="text-center">Դիտել ավելին</th>
                            <th class="text-center">Վճարել</th>
                        @endif
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($studentPrices as $k=>$studentPrice)
                        <tr>
                            <td>{{$k+1}} Ամիս</td>
                            <td>{{$student->fullName ?? ' '}}</td>
                            <td>{{$student->fullName ?? ' '}}</td>
                            <td>{{$studentPrice->start_date}}</td>
                            @if($data=="on")
                                <td>{{$studentPrice->end_date}}</td>
                            @else
                                <td>{{$studentPrice->day}}</td>
                            @endif
                            <td>{{$studentPrice->price? $studentPrice->price.' դրամ':$studentPrice->price_full.' դրամ' }}</td>
                            @if($data=="on")
                                @if($studentPrice->month=='active')
                            <td class="text-center"><span class="text-success">Գործող</span></td>
                                @else
                            <td class="text-center"><span class="text-danger">Ավարտված</span></td>
                                @endif
                            @endif
                            @if($data=="on")
                                <td class="text-center"><a href="{{route('admin.StudentList.show',$studentPrice->id)}}">View</a>
                                </td>
                                <td class="text-center" onclick="location.href='{{route('admin.StudentNewMath.showThisStudent',$student->id)}}'">
                                    <button class="form-control">Վճարել</button>
                                </td>

                            @endif

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link href=" {{asset('admin_style/assets/css/tables/table-basic.css')}}" rel="stylesheet" type="text/css">
@endsection
