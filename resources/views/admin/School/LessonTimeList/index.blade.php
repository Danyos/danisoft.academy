@extends('admin.layouts.admin')
@section('content')
    <?php $routerPathCreted = "admin.roles.create";
    $settings = false;
    $section_menu = "Վճարումներ";
    $section_menu_url = "admin.LessonTimeList.index";


    ?>

    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="row layout-top-spacing">
                <div class="col-xl-12 col-lg-12 col-md-12">

                    @if (session()->has('price_debt'))

                        <div class="alert alert-arrow-left alert-icon-left alert-light-primary mb-4" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <svg xmlns="http://www.w3.org/2000/svg" data-dismiss="alert" width="24" height="24"
                                     viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                     stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-bell">
                                <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                                <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                            </svg>
                            <strong>Տեղեկացում!</strong> {!! \Session::get('price_debt')['price_debt'] !!} դրամ
                        </div>
                    @endif


                    <div class="mail-box-container">
                        <div class="mail-overlay"></div>

                        <div class="tab-title">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-12 text-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-clipboard">
                                        <path
                                            d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path>
                                        <rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect>
                                    </svg>
                                    <h5 class="app-title">Վճարումներ</h5>
                                </div>

                                <div class="todoList-sidebar-scroll">
                                    <div class="col-md-12 col-sm-12 col-12 mt-4 pl-0">
                                        <ul class="nav nav-pills d-block" id="pills-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link list-actions {{$status=='all' ?'active':''}}"
                                                   id="todo-task-done"
                                                   onclick="doAlert(2)" data-toggle="pill" href="#pills-sentmail"
                                                   role="tab" aria-selected="false">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-file-text flaticon-menu-list">
                                                        <path
                                                            d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                        <polyline points="14 2 14 8 20 8"></polyline>
                                                        <line x1="16" y1="13" x2="8" y2="13"></line>
                                                        <line x1="16" y1="17" x2="8" y2="17"></line>
                                                        <polyline points="10 9 9 9 8 9"></polyline>
                                                    </svg>
                                                    Ամբողջ <label for="nosales" class="todo-badge badge"></label></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link list-actions {{$status=='active' ?'active':''}} "
                                                   id="all-list"
                                                   onclick="doAlert(1)" data-toggle="pill" href="#pills-inbox"
                                                   role="tab" aria-selected="true">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-list">
                                                        <line x1="8" y1="6" x2="21" y2="6"></line>
                                                        <line x1="8" y1="12" x2="21" y2="12"></line>
                                                        <line x1="8" y1="18" x2="21" y2="18"></line>
                                                        <line x1="3" y1="6" x2="3" y2="6"></line>
                                                        <line x1="3" y1="12" x2="3" y2="12"></line>
                                                        <line x1="3" y1="18" x2="3" y2="18"></line>
                                                    </svg>
                                                    Վճարված <label for="yessales" class="todo-badge badge"></label></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link list-actions {{$status=='inactive' ?'active':''}}"
                                                   id="todo-task-done"
                                                   onclick="doAlert(0)" data-toggle="pill" href="#pills-sentmail"
                                                   role="tab" aria-selected="false">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-file-text flaticon-menu-list">
                                                        <path
                                                            d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                        <polyline points="14 2 14 8 20 8"></polyline>
                                                        <line x1="16" y1="13" x2="8" y2="13"></line>
                                                        <line x1="16" y1="17" x2="8" y2="17"></line>
                                                        <polyline points="10 9 9 9 8 9"></polyline>
                                                    </svg>
                                                    Չվճարված <label for="nosales" class="todo-badge badge"></label></a>
                                            </li>

                                        </ul>

                                        <form action="{{route('admin.LessonTimeList.store')}}" method="post">

                                            @csrf
                                            <input type="radio" id="sales2" hidden
                                                   {{$status=='all' ?'checked':''}} name="sales" value="all">
                                            <input type="radio" id="sales1" hidden
                                                   {{$status=='active' ?'checked':''}} name="sales" value="active">
                                            <input type="radio" id="sales0" hidden
                                                   {{$status=='inactive' ?'checked':''}} name="sales" value="inactive">
                                            <select name="folder" id="" class="form-control">
                                                @foreach($folder as $folders)
                                                    <option
                                                        value="{{$folders->id}}" {{$folderFirst->id==$folders->id ? 'selected' :''}} >{{$folders->title}}</option>
                                                @endforeach
                                            </select>
                                            <br>
                                            <input type="submit" class="form-control" value="Դիտել արդյունք">
                                        </form>
                                    </div>
                                </div>
                                <script>
                                    function doAlert(checkboxElem) {
                                        var a = document.getElementById('sales' + checkboxElem)


                                        document.getElementById('sales0').removeAttribute('checked')
                                        document.getElementById('sales1').removeAttribute('checked')
                                        document.getElementById('sales2').removeAttribute('checked')

                                        a.setAttribute("checked", "checked");


                                    }
                                </script>
                                <a class="btn" id="addTask" data-toggle="modal" data-target="#exampleModal">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-plus">
                                        <line x1="12" y1="5" x2="12" y2="19"></line>
                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                    </svg>
                                    Նոր խումբ</a>
                            </div>
                        </div>
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Ավելացնել նոր դասաժամ</h5>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{route('admin.LessonTimeList.addLesson')}}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <label for="title">Անվանում</label>
                                                <input type="text" name="title" id="title" class="form-control"
                                                       required>
                                            </div>
                                            <div class="form-group">
                                                <label for="online">Դասի տեսակը</label>
                                                <br>
                                                <label for="online">Online</label>
                                                <input type="radio" name="type" id="online" value="online" checked>
                                                <label for="offline">Offline</label>
                                                <input type="radio" name="type" id="offline" value="offline">

                                            </div>
                                            <div class="form-group">
                                                <label for="online">Դասավանդ</label>
                                                <select name="user_id" id="" class="form-control">
                                                    @foreach($user as $users)
                                                        <option value="{{$users->id}}">{{$users->name}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>


                                        </form>


                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i>
                                            Փակել
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="exampleModal{{$folderFirst->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Ավելացնել նոր Ուսանող</h5>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{route('admin.StudentList.store')}}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <label for="fullName">Անուն</label>
                                                <input type="text" name="fullName" id="fullName" class="form-control"
                                                       required>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Փոստ</label>
                                                <input type="text" name="email" id="email" class="form-control"
                                                       required>
                                            </div>
                                            <div class="form-group">
                                                <label for="age">Տարիք</label>
                                                <input type="text" name="age" id="age" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Հեռախոս</label>
                                                <input type="text" name="phone" id="phone" class="form-control"
                                                       required>
                                            </div>
                                            <div class="form-group">
                                                <label for="price">Վարձավճար</label>
                                                <input type="text" name="price" id="price" class="form-control"
                                                       required>
                                            </div>
                                            <div class="form-group">
                                                <label for="start_date">Երբ է սկսել</label>
                                                <input type="date" name="start_date" id="start_date"
                                                       class="form-control" required>
                                            </div>
                                            <input type="hidden" name="group_id" value="{{$folderFirst->id}}">

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>


                                        </form>


                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i>
                                            Փակել
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="todo-inbox" class="accordion todo-inbox">
                            <div class="search">
                                <button class="btn btn-danger" onclick="takeStudent()">Move</button>
                                <div style="display: none;width: 80%;" class="search" id="takeFolder">
                                <span style="display:inline-block; width: 40%;">
                                <select name="folder" id="changstudentfolder" class="form-control" style="width: 100%;">
                                    @foreach($folder as $folders)
                                        <option
                                            value="{{$folders->id}}" {{$folderFirst->id==$folders->id ? 'selected' :''}} >{{$folders->title}}
                                        </option>
                                    @endforeach
                                </select>

                                </span>
                                    <span style="display:inline-block; width: 30%;">

                                <button class="btn btn-success" onclick="move()"
                                        style="display: inline-block">Հաստատել</button>
                                </span>
                                </div>

                                <a class="btn btn-secondary " id="addTask" data-toggle="modal"
                                   data-target="#exampleModal{{$folderFirst->id}}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-plus">
                                        <line x1="12" y1="5" x2="12" y2="19"></line>
                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                    </svg>
                                </a>

                            </div>


                            <div class="todo-box">

                                <div id="ct" class="todo-box-scroll">
                                    @foreach($student as $students)

                                        <div class="todo-item all-list" id="student{{$students->id}}">
                                            <div class="todo-item-inner">
                                                <div class="n-chk text-center">
                                                    <label class="new-control new-checkbox checkbox-primary">
                                                        <input type="checkbox" class="new-control-input inbox-chkbox students{{$students->id}}"
                                                               id="muveStudent" name="move[]" value="{{$students->id}}">
                                                        <span class="new-control-indicator"></span>
                                                    </label>
                                                </div>

                                                <div class="todo-content">
                                                    <h5 class="todo-heading"
                                                        data-todoHeading="Meeting with Shaun Park at 4:50pm">{{$students->fullName}}</h5>
                                                    <p class="meta-date">{{$students->start_date}}</p>
                                                    <p class="todo-text"
                                                       data-todoHtml="<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.</p>"
                                                       data-todoText='{"ops":[{"insert":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.\n"}]}'>
                                                        Փոստ։ {{$students->email}}<br>
                                                        Տարիք։ {{$students->age}}<br>
                                                        Հեռախոսահամար։ {{$students->phone}}<br>
                                                        Գումար։ {{$students->price}}<br>

                                                    </p>
                                                </div>

                                                <div class="priority-dropdown custom-dropdown-icon">
                                                    <div class="dropdown p-dropdown">

                                                        @isset($students->StudentPrice->status)
                                                            @if($students->StudentPrice->status==='inactive')

                                                                @include('admin.School.LessonTimeList.include.inactive')
                                                            @else

                                                                @include('admin.School.LessonTimeList.include.active')
                                                            @endif


                                                            @include('admin.School.LessonTimeList.include.AddPrice')

                                                        @else


                                                            @include('admin.School.LessonTimeList.include.NewMonth')

                                                        @endisset
                                                    </div>
                                                </div>

                                                <div class="action-dropdown custom-dropdown-icon">
                                                    <div class="dropdown">
                                                        <a class="dropdown-toggle" href="#" role="button"
                                                           id="dropdownMenuLink-2" data-toggle="dropdown"
                                                           aria-haspopup="true" aria-expanded="true">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                 height="24"
                                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                                 stroke-width="2" stroke-linecap="round"
                                                                 stroke-linejoin="round"
                                                                 class="feather feather-more-vertical">
                                                                <circle cx="12" cy="12" r="1"></circle>
                                                                <circle cx="12" cy="5" r="1"></circle>
                                                                <circle cx="12" cy="19" r="1"></circle>
                                                            </svg>
                                                        </a>

                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink-2">
                                                            <a class="important dropdown-item"
                                                               href="{{route('admin.StudentOfDaniSoft.show',$students->id)}}">Վճարումներ</a>
                                                            <form action="{{route('admin.StudentOfDaniSoft.destroy',$students->id)}}" method="post">
                                                                @csrf
                                                                @method('delete')
                                                            <button class="dropdown-item delete" type="submit">Ջնջել</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    @endforeach


                                </div>

                                <div class="modal fade" id="todoShowListItem" tabindex="-1" role="dialog"
                                     aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-x close" data-dismiss="modal">
                                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                                </svg>
                                                <div class="compose-box">
                                                    <div class="compose-content">
                                                        <h5 class="task-heading"></h5>
                                                        <p class="task-text"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn" data-dismiss="modal">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-trash">
                                                        <polyline points="3 6 5 6 21 6"></polyline>
                                                        <path
                                                            d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                    </svg>
                                                    Close
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="addTaskModal" tabindex="-1" role="dialog"
                         aria-labelledby="addTaskModalTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-x close" data-dismiss="modal">
                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                    </svg>
                                    <div class="compose-box">
                                        <div class="compose-content" id="addTaskModalTitle">
                                            <h5 class="">Add Task</h5>
                                            <form>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="d-flex mail-to mb-4">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                 height="24" viewBox="0 0 24 24" fill="none"
                                                                 stroke="currentColor" stroke-width="2"
                                                                 stroke-linecap="round" stroke-linejoin="round"
                                                                 class="feather feather-edit-3 flaticon-notes">
                                                                <path d="M12 20h9"></path>
                                                                <path
                                                                    d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>
                                                            </svg>
                                                            <div class="w-100">
                                                                <input id="task" type="text" placeholder="Task"
                                                                       class="form-control" name="task">
                                                                <span class="validation-text"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="d-flex  mail-subject mb-4">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-file-text flaticon-menu-list">
                                                        <path
                                                            d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                        <polyline points="14 2 14 8 20 8"></polyline>
                                                        <line x1="16" y1="13" x2="8" y2="13"></line>
                                                        <line x1="16" y1="17" x2="8" y2="17"></line>
                                                        <polyline points="10 9 9 9 8 9"></polyline>
                                                    </svg>
                                                    <div class="w-100">
                                                        <div id="taskdescription" class=""></div>
                                                        <span class="validation-text"></span>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard
                                    </button>
                                    <button class="btn add-tsk">Add Task</button>
                                    <button class="btn edit-tsk">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="footer-wrapper">
            <div class="footer-section f-section-1">
                <p class="">Copyright © 2020 <a target="_blank" href="https://designreset.com/">DesignReset</a>, All
                    rights reserved.</p>
            </div>
            <div class="footer-section f-section-2">
                <p class="">Coded with
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-heart">
                        <path
                            d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
                    </svg>
                </p>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        function takeStudent() {
            var cboxes = document.getElementsByName('move[]');
            let takeFolder = document.getElementById('takeFolder');
            var len = cboxes.length;
            let studentMove = [];

            for (var i = 0; i < len; i++) {
                if (cboxes[i].checked) {
                    studentMove.push(cboxes[i].value);

                }
            }
            // console.log(studentMove)

            if (studentMove.length > 0) {


                takeFolder.style.display = 'block'
            } else {
                takeFolder.style.display = 'none'
            }
            return studentMove;
        }

        function move() {
            var studentMove = takeStudent();
            var folder = document.getElementById('changstudentfolder').value
            // console.log(studentMove);
            fetch("{{route('admin.LessonFolderUpdate')}}", {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-Token": $('input[name="_token"]').val()
                },
                method: "post",
                credentials: "same-origin",
                body: JSON.stringify({
                    key: studentMove,
                    folder: folder
                })
            }).then(function (response) {
                return response.json();
            }).then(function (json) {

                for (var stid of json) {
                    document.getElementById('student' + stid).style.display = 'none';
                    document.getElementsByClassName('students' + stid)[0].checked = false;
                    let takeFolder = document.getElementById('takeFolder');
                    takeFolder.style.display = 'none'
                }

                // student

            })
                .catch(function (error) {


                });
        }
    </script>

@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('admin_style/plugins/editors/quill/quill.snow.css')}}">
    <link href="{{asset('admin_style/assets/css/apps/todolist.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('admin_style/assets/css/elements/alert.css')}}">
    <style>
        .btn-light {
            border-color: transparent;
        }
    </style>
    <link href="{{asset('admin_style/assets/css/components/custom-modal.css')}}" rel="stylesheet" type="text/css"/>
@endsection
