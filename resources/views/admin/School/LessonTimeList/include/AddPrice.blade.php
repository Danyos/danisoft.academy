<div class="dropdown-menu"
     aria-labelledby="{{$students->StudentPrice->status ?? ' '}}-{{$students->StudentPrice->id ?? ' '}}">

    <a href=""
       class="dropdown-item primary">{{$students->StudentPrice->price_full ?? ' '}}
        դրամ</a>
    <a href=""
       class="dropdown-item">{{$students->StudentPrice->day!=null ? $students->StudentPrice->day.' '. 'օր':''}}</a>
    <a href="" class="dropdown-item">
        {{$students->StudentPrice->start_date}}
        <br> {{$students->StudentPrice->end_date}}</a>
    @if($students->StudentPrice->status=='inactive')
        <form
            action="{{route('admin.LessonTimeList.update',$students->id)}}"
            method="post"
            id="priceUppdate{{$students->StudentPrice->id}}">
            @csrf
            @method('put')
            <input type="number" id="price"
                   placeholder="Գումար" name="price">
            <br>
            <br>
            <label for="whatdate"><abbr
                    title=" Վճարելէ ամբողջությամբ">Վճար</abbr></label>
            <input type="checkbox" id="whatdate" name="priceall"
                   value="ok">
            <br>
            <br>
            <a class="dropdown-item primary"
               onclick="document.getElementById('priceUppdate'+{{$students->StudentPrice->id}}).submit()"
            >
                <svg xmlns="http://www.w3.org/2000/svg"
                     width="24" height="24" viewBox="0 0 24 24"
                     fill="none" stroke="currentColor"
                     stroke-width="2" stroke-linecap="round"
                     stroke-linejoin="round"
                     class="feather feather-check">
                    <polyline
                        points="20 6 9 17 4 12"></polyline>
                </svg>

                Վճարված</a>
        </form>
    @else
        <a class="dropdown-item primary"
           href="{{route('admin.StudentNewMath.new.math',$students->id)}}">
            <svg xmlns="http://www.w3.org/2000/svg"
                 width="24" height="24" viewBox="0 0 24 24"
                 fill="none" stroke="currentColor"
                 stroke-width="2" stroke-linecap="round"
                 stroke-linejoin="round"
                 class="feather feather-check">
                <polyline
                    points="20 6 9 17 4 12"></polyline>
            </svg>

            Ստեղծել նոր ամիս</a>
    @endif
</div>
