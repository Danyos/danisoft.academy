<a class="dropdown-toggle warning" href="#" role="button"
   id="{{$students->id}}-{{$students->age}}"
   data-toggle="dropdown"
   aria-haspopup="true" aria-expanded="true">
    <svg xmlns="http://www.w3.org/2000/svg" width="24"
         height="24"
         viewBox="0 0 24 24" fill="none"
         stroke="currentColor"
         stroke-width="2" stroke-linecap="round"
         stroke-linejoin="round"
         class="feather feather-alert-octagon">
        <polygon
            points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon>
        <line x1="12" y1="8" x2="12" y2="12"></line>
        <line x1="12" y1="16" x2="12" y2="16"></line>
    </svg>
</a>
<div class="dropdown-menu"
     aria-labelledby="{{$students->id?? ' '}}-{{$students->age ?? ' '}}">

    <a href=""
       class="dropdown-item primary">0
        դրամ</a>

        <form
            action="{{route('admin.LessonTimeList.update',$students->id)}}"
            method="post"
            id="priceUppdates{{$students->id}}">
            @csrf
            @method('put')
            <input type="number" id="price"
                   placeholder="Գումար" name="price">
            <br>
            <br>
            <label for="whatdate"><abbr
                    title=" Վճարելէ ամբողջությամբ">Վճար</abbr></label>
            <input type="checkbox" id="whatdate" name="priceall"
                   value="ok">
            <br>
            <br>
            <a class="dropdown-item primary"
               onclick="document.getElementById('priceUppdates'+{{$students->id}}).submit()"
            >
                <svg xmlns="http://www.w3.org/2000/svg"
                     width="24" height="24" viewBox="0 0 24 24"
                     fill="none" stroke="currentColor"
                     stroke-width="2" stroke-linecap="round"
                     stroke-linejoin="round"
                     class="feather feather-check">
                    <polyline
                        points="20 6 9 17 4 12"></polyline>
                </svg>

                Վճարված</a>
        </form>

</div>
