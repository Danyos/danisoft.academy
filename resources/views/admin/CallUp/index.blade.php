@extends('admin.layouts.admin')
@section('css')
    <link href="{{asset('admin_style/assets/css/apps/notes.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin_style/assets/css/forms/theme-checkbox-radio.css')}}" rel="stylesheet" type="text/css"/>
    <style>
        .validation-text {
            color: red;
        }
    </style>
@endsection
@section('js')
{{--        <script src="{{asset('admin_style/assets/js/ie11fix/fn.fix-padStart.js')}}"></script>--}}
{{--    <script src="{{asset('admin_style/assets/js/apps/notes.js')}}"></script>--}}
    <script src="{{asset('admin_style/js/fetch.js')}}"></script>

    <script src="{{asset('admin_style/assets/js/apps/notes.js')}}"></script>
    <script>
        function save(data) {
            var token = $("input[name='_token']").val();
            var url = "{{route('admin.registerLesson.store')}}"
            var method='post'
            fetchsend(token, url,method, data,'htmltype')
        }
        function trashnotes(ids){
            let con=confirm("Press a button!");
            if (con){
                var token = $("input[name='_token']").val();
                var url = "{{url('admin/CallUpDelete/')}}/"+ids
                var method='post'
                fetchsend(token, url, method,ids,'datatrash')
            }

        }
function typenotes(data){
            console.log(data)
    var token = $("input[name='_token']").val();
    var url = "{{route('admin.notes.update.type')}}"
    var method='post'
    fetchsend(token, url,method, data,'NotesUpdate')
}
function typeFav(data){

    var token = $("input[name='_token']").val();
    var url = "{{url('admin/CallUp/favorite/type/')}}/"+data.id
    var method='post'
    fetchsend(token, url,method, data,'LessonTypeFavUpdate')
}
function NotesUpdate(json){

    var classNames=document.getElementById('trash'+json.id).classList
       var thisclass=classNames[2]
    classNames.remove(thisclass)
        classNames.add("note-"+json['type']);

    // console.log(classNames)
}
function LessonTypeFavUpdate(json){
console.log(json)
    var favNames=document.getElementById('fav'+json.id)
if (json.fav=='active'){
    favNames.setAttribute('style', 'color:green !important;cursor: pointer;')
}else{
    favNames.setAttribute('style', 'color:red !important;cursor: pointer;')
}

    // console.log(classNames)
}

        function typeStatus(data){

            var token = $("input[name='_token']").val();
            var url = "{{url('admin/CallUp/status/')}}/"+data.id
            var method='post'
            fetchsend(token, url,method, data,'LessonTypeStatusUpdate')
        }
function LessonTypeStatusUpdate(json){
console.log(json)
    var favNames=document.getElementById('status'+json.id)
if (json.status=='active'){
    favNames.setAttribute('style', 'color:red !important;cursor: pointer;')
}else{
    favNames.setAttribute('style', 'color:green !important;cursor: pointer;')
}

    // console.log(classNames)
}
    </script>
@endsection
@section('content')

    <?php
    $section_menu = "Հուշատետր";
    $section_menu_url = "admin.notes.index";


    ?>

    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="row app-notes layout-top-spacing" id="cancel-row">
                <div class="col-lg-12">
                    <div class="app-hamburger-container">
                        <div class="hamburger">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-menu chat-menu d-xl-none">
                                <line x1="3" y1="12" x2="21" y2="12"></line>
                                <line x1="3" y1="6" x2="21" y2="6"></line>
                                <line x1="3" y1="18" x2="21" y2="18"></line>
                            </svg>
                        </div>

                    </div>

                    <div class="app-container">

                        <div class="app-note-container">

                            <div class="app-note-overlay"></div>

                            <div class="tab-title">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-12 text-center">
                                        <a id="btn-add-notes" class="btn btn-primary" href="javascript:void(0);">Ավելացնել</a>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-12 mt-5">
                                        <h2 align="center">{{$callUpCount}}</h2>
                                        <ul class="nav nav-pills d-block" id="pills-tab3" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link list-actions {{ request()->is('admin/registerLesson/all') ? 'active' : '' }}" id="all-notes"  href="{{route('admin.CallUp.show','all')}}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-edit">
                                                        <path
                                                            d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                                        <path
                                                            d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                                    </svg>
                                                    Բոլոր</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link list-actions {{ request()->is('admin/CallUp/active') ? 'active' : '' }}" id="all-notes"  href="{{route('admin.CallUp.show','active')}}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-edit">
                                                        <path
                                                            d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                                        <path
                                                            d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                                    </svg>
                                                    Բացված</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link list-actions {{ request()->is('admin/CallUp/inactive') ? 'active' : '' }}" id="all-notes"  href="{{route('admin.CallUp.show','inactive')}}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-edit">
                                                        <path
                                                            d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                                        <path
                                                            d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                                    </svg>
                                                    Չբացված</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link list-actions {{ request()->is('admin/CallUp/fav')  ? 'active' : '' }}" id="note-fav" href="{{route('admin.CallUp.show','fav')}}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-star">
                                                        <polygon
                                                            points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
                                                    </svg>
                                                    Հիշել</a>
                                            </li>
                                        </ul>

                                        <hr/>

                                        <p class="group-section">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-tag">
                                                <path
                                                    d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path>
                                                <line x1="7" y1="7" x2="7" y2="7"></line>
                                            </svg>
                                           <span style="font-size: 10px;">Մեզ Զանգահարել են</span>
                                        </p>

                                        <ul class="nav nav-pills d-block group-list active" id="pills-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link list-actions g-dot-primary {{ request()->is('admin/CallUp/client') ? 'active' : '' }}" id="note-personal" href="{{route('admin.CallUp.show','client')}}" >Բոլոր</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link list-actions g-dot-danger {{ request()->is('admin/CallUp/clientInactive') ? 'active' : '' }}"  href="{{route('admin.CallUp.show','clientInactive')}}" id="note-important">Ակտիվ</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link list-actions g-dot-success {{ request()->is('admin/CallUp/clientActive') ? 'active' : '' }}" href="{{route('admin.CallUp.show','clientActive')}}"
                                                   id="note-social">Բացված</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link list-actions g-dot-warning {{ request()->is('admin/CallUp/clientFavorite') ? 'active' : '' }}" href="{{route('admin.CallUp.show','clientFavorite')}}" id="note-work">Հիշել</a>
                                            </li>


                                        </ul>
                                    </div>
                                </div>
                            </div>


                            <div id="ct" class="note-container note-grid">
                                @foreach($callUp as $note)

                                    <div class="note-item all-notes note-{{$note->status}}" id="trash{{$note->id}}">
                                        <div class="note-inner-content">
                                            <div class="note-content">
{{--                                                {{$note->id}}--}}
                                                <p class="note-title"
                                                   data-noteTitle="Meeting with Kelly">{{$note->ClientName}}</p>

                                                <p class="meta-time">{{$note->phone}}</p>
                                                <p class="meta-time">{{$note->client}}</p>
                                                <br>
                                                <p class="meta-time">{{$note->created_at}}</p><br>
                                                <p class="meta-time" style="color:red">{{$note->slug}}</p>
                                                <div class="note-description-content">
                                                    <p class="note-description"
                                                       data-noteDescription="{{$note->comments}}">{{$note->comments}}</p>
                                                </div>
                                            </div>
                                            <div class="note-action">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-star  " style="cursor: pointer;{{$note->favorite=='active'?'color:green;':'color:red'}}" id="fav{{$note->id}}" onclick="typeFav({'fav':'active','id':{{$note->id}} } )">
                                                    <polygon
                                                        points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-trash-2 delete-note" onclick="trashnotes({{$note->id}})">
                                                    <polyline points="3 6 5 6 21 6"></polyline>
                                                    <path
                                                        d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                    <line x1="10" y1="11" x2="10" y2="17"></line>
                                                    <line x1="14" y1="11" x2="14" y2="17"></line>
                                                </svg>
                                                <svg  width="24" height="24" viewBox="0 0 24 24" fill="red"  stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather" id="status{{$note->id}}" onclick="typeStatus({'status':'active','id':{{$note->id}} } )" style="color:{{($note->status=='active')? "red": "green" }}">
                                                    <path
                                                        d="M10.219,1.688c-4.471,0-8.094,3.623-8.094,8.094s3.623,8.094,8.094,8.094s8.094-3.623,8.094-8.094S14.689,1.688,10.219,1.688 M10.219,17.022c-3.994,0-7.242-3.247-7.242-7.241c0-3.994,3.248-7.242,7.242-7.242c3.994,0,7.241,3.248,7.241,7.242C17.46,13.775,14.213,17.022,10.219,17.022 M15.099,7.03c-0.167-0.167-0.438-0.167-0.604,0.002L9.062,12.48l-2.269-2.277c-0.166-0.167-0.437-0.167-0.603,0c-0.166,0.166-0.168,0.437-0.002,0.603l2.573,2.578c0.079,0.08,0.188,0.125,0.3,0.125s0.222-0.045,0.303-0.125l5.736-5.751C15.268,7.466,15.265,7.196,15.099,7.03"></path>
                                                </svg>
                                            </div>
                                            <div class="note-footer">
                                                <div class="tags-selector btn-group">
                                                    <a class="nav-link dropdown-toggle d-icon label-group"
                                                       data-toggle="dropdown" href="#" role="button"
                                                       aria-haspopup="true" aria-expanded="true">
                                                        <div class="tags">

                                                            <div class="g-dot-{{$note->type}}" ></div>

                                                        </div>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right d-icon-menu">
                                                        <a class="note-personal label-group-item label-personal dropdown-item position-relative g-dot-personal"
                                                           href="javascript:void(0);" onclick="typenotes({'type':'personal','id':{{$note->id}} } )"> Ստանդարտ</a>
                                                        <a class="note-work label-group-item label-work dropdown-item position-relative g-dot-work"
                                                           href="javascript:void(0);" onclick="typenotes({'type':'work','id':{{$note->id}} } )"> Աշխատանքաին</a>
                                                        <a class="note-social label-group-item label-social dropdown-item position-relative g-dot-social"
                                                           href="javascript:void(0);" onclick="typenotes({'type':'social','id':{{$note->id}} } )"> Կիսատ վճար</a>
                                                        <a class="note-important label-group-item label-important dropdown-item position-relative g-dot-important"
                                                           href="javascript:void(0);" onclick="typenotes({'type':'important','id':{{$note->id}} } )"> Կարևոր</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            </div>

                        </div>

                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="notesMailModal" tabindex="-1" role="dialog"
                         aria-labelledby="notesMailModalTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-x close" data-dismiss="modal">
                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                    </svg>
                                    <div class="notes-box">
                                        <div class="notes-content">
                                            <form action="{{route('admin.CallUp.store')}}" method="post" id="notesMailModalTitle">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="d-flex note-title">
                                                            <input type="text" id="n-title" name="ClientName" class="form-control"
                                                                   maxlength="25" placeholder="Title">
                                                        </div>
                                                        <span class="validation-text" id="validation-text-title"></span>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="d-flex note-title">
                                                            <input type="number" id="n-title" name="phone" class="form-control"
                                                                   maxlength="25" placeholder="Հեռախոսահամար">
                                                        </div>
                                                        <span class="validation-text" id="validation-text-title"></span>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="d-flex note-description">
                                                            <textarea id="n-description" name="comments" class="form-control"
                                                                      placeholder="Ինչ եք խոսել"></textarea>
                                                        </div>
                                                        <span class="validation-text" id="validation-text-desc"></span>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="d-flex note-description">
                                                            <input hidden name="client" value="admin">

                                                        </div>

                                                    </div>
                                                </div>
                                                <br>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn">Add
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="footer-wrapper">
            <div class="footer-section f-section-1">
                <p class="">Copyright © 2020 <a target="_blank" href="https://designreset.com/">DesignReset</a>, All
                    rights reserved.</p>
            </div>
            <div class="footer-section f-section-2">
                <p class="">Coded with
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-heart">
                        <path
                            d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
                    </svg>
                </p>
            </div>
        </div>
    </div>
    <!--  END CONTENT AREA  -->

@endsection
