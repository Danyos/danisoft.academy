@extends('admin.layouts.admin')
@section('css')

@endsection
@section('js')

@endsection
@section('content')

    <?php
    $section_menu_section_create = "Ավելացնել";
    $section_menu = "Օգտատեր";
    $section_menu_url = "admin.users.index";
    ?>
    <div id="content" class="main-content">
        <div class="layout-px-spacing">
            <br>
            @if(session()->has('password_error'))
                <strong style="color: red;">Լրացրեք գաղտնաբարը</strong>
            @endif
            <form action="{{ route("admin.users.update", [$user->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">{{ trans('global.user.fields.name') }}*</label>
                    <input type="text" id="name" name="name" class="form-control"
                           value="{{ old('name', isset($user) ? $user->name : '') }}">
                    @if($errors->has('name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('global.user.fields.name_helper') }}
                    </p>
                </div>
                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label for="email">{{ trans('global.user.fields.email') }}*</label>
                    <input type="email" id="email" name="email" class="form-control"
                           value="{{ old('email', isset($user) ? $user->email : '') }}">
                    @if($errors->has('email'))
                        <em class="invalid-feedback">
                            {{ $errors->first('email') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('global.user.fields.email_helper') }}
                    </p>
                </div>
                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label for="password">{{ trans('global.user.fields.password') }}</label>
                    <input type="password" id="password" name="password" class="form-control">
                    @if($errors->has('password'))
                        <em class="invalid-feedback">
                            {{ $errors->first('password') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('global.user.fields.password_helper') }}
                    </p>
                </div>
                @if(\Illuminate\Support\Facades\Auth::user()->roles[0]->title=='Admin')
                    <div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }}">
                        <label for="roles">{{ trans('global.user.fields.roles') }}*


                            <select name="roles[]" id="roles" class="form-control ">
                                @foreach($roles as $id => $roles)
                                    <option
                                        value="{{ $id }}" {{ (in_array($id, old('roles', [])) || isset($user) && $user->roles->contains($id)) ? 'selected' : '' }}>
                                        {{ $roles }}
                                    </option>
                                @endforeach
                            </select>

                            @if($errors->has('roles'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('roles') }}
                                </em>
                            @endif

                            <p class="helper-block">
                                {{ trans('global.user.fields.roles_helper') }}
                            </p>
                    </div>
                @else
                    <select name="roles[]" id="roles" class="form-control " hidden>
                        @foreach($roles as $id => $roles)
                            <option
                                value="{{ $id }}" {{ (in_array($id, old('roles', [])) || isset($user) && $user->roles->contains($id)) ? 'selected' : '' }}>
                                {{ $roles }}
                            </option>
                        @endforeach
                    </select>
                @endif
                <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label for="name">Avatar*</label>
                    <input type="file" id="name" name="avatar" class="form-control">
                    @if($errors->has('avatar'))
                        <em class="invalid-feedback">
                            {{ $errors->first('avatar') }}
                        </em>
                    @endif
                </div>
                <div>
                    <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                </div>
            </form>
            <img src="{{asset($user->avatar)}}" alt="" width="300px">
        </div>
    </div>

@endsection
