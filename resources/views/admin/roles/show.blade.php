@extends('admin.layouts.admin')
@section('content')
    <?php
    $settings=true;
    $routerPathCreted="admin.roles.create";
    $routerPathedit="admin.roles.edit";
    $this_id=$role->id;
    $section_menu="Պաշտոն";

    $section_menu_url="admin.roles.index";
    $section_menu_section_show="Դիտել";
    ?>

    <div id="content" class="main-content">
        <div class="layout-px-spacing">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr style="color: white;">
                    <th>
                        {{ trans('global.role.fields.title') }}
                    </th>
                    <td>
                        {{ $role->title }}
                    </td>
                </tr>
                <tr style="color: white;">
                    <th >
                        Permissions
                    </th>
                    <td>
                        @foreach($role->permissions as $id => $permissions)
                            <span class="label label-info label-many"> {{ $permissions->title }},</span>
                        @endforeach
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection
