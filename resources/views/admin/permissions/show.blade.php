@extends('admin.layouts.admin')
@section('content')

    <?php
    $settings=true;
    $routerPathCreted="admin.permissions.create";
    $routerPathedit="admin.permissions.edit";
    $this_id=$permission->id;
    $section_menu="Թույլատվություն";
    $section_menu_url="admin.permissions.index";
    $section_menu_section_show="Դիտել";
    ?>

    <div id="content" class="main-content" style="">
        <div class="layout-px-spacing">
            <br>
            <table class="table table-bordered table-striped" style="color: white">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.permission.fields.title') }}
                    </th>
                    <td>
                        {{ $permission->title }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection
