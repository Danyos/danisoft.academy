<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from designreset.com/cork/ltr/demo3/auth_login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Jan 2021 11:51:22 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Danisoft </title>
    <meta name="description" content="Ուսուցման գործընթացը հիմնված է գործնական խնդիրների և ուսուցչի հետ շփման վրա: Մենք միանման դասախոսություններ չունենք: Եվ գիտելիքն անմիջապես գործնականում ամրագրված է:">

    <link rel="icon" type="image/x-icon" href="{{asset('clent/images/DaniSoft.svg')}}"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="{{asset('admin_style/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin_style/assets/css/plugins.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin_style/assets/css/authentication/form-1.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset('admin_style/assets/css/forms/theme-checkbox-radio.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin_style/assets/css/forms/switches.css')}}">
</head>
<body class="form">


<div class="form-container">
    <div class="form-form">
        <div class="form-form-wrap">
            <div class="form-container">
                <div class="form-content">

                    <h1 class="">Log In to <a href="{{route('client.index')}}"><span class="brand-name">DaniSoft</span></a></h1>

                    <form class="text-left" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form">

                            <div id="username-field" class="field-wrapper input">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                <input id="username" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Username">
                            </div>

                            <div id="password-field" class="field-wrapper input mb-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>
                                <input id="password"  type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="d-sm-flex justify-content-between">
                                <div class="field-wrapper toggle-pass">

                                    <label class="new-control new-checkbox checkbox-outline-primary">
                                        <input type="checkbox" class="new-control-input" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <span class="new-control-indicator"></span>Keep me logged in
                                    </label>
                                </div>
                                <div class="field-wrapper">
                                    <button type="submit" class="btn btn-primary" value="">Log In</button>
                                </div>

                            </div>
                        </div>
                    </form>
                    <p class="terms-conditions">© 2021 All Rights Reserved. <a href="{{route('client.index')}}">DaniSoft</a> is a product of Designreset. <a href="javascript:void(0);">Cookie Preferences</a>, <a href="javascript:void(0);">Privacy</a>, and <a href="javascript:void(0);">Terms</a>.</p>

                </div>
            </div>
        </div>
    </div>
    <div class="form-image">
        <div class="l-image">
        </div>
    </div>
</div>


<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{asset('admin_style/assets/js/libs/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('admin_style/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('admin_style/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- END GLOBAL MANDATORY SCRIPTS -->
<script src="{{asset('admin_style/assets/js/authentication/form-1.js')}}"></script>

</body>

<!-- Mirrored from designreset.com/cork/ltr/demo3/auth_login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Jan 2021 11:51:22 GMT -->
</html>
