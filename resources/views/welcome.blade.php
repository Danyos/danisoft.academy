@extends("layouts.base")
@section('menu')

@endsection
@section('content')

    <section class="home-slider-area-2">
        @foreach($sliders as $slider)
            <div class="slider-item overlay-black" style="background: url({{asset($slider->avatar)}})">
                <div class="slider-item-table">
                    <div class="slider-item-tablecell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5 col-sm-5">
                                    <div class="slider-text">
                                        <h3>{{$slider->subject}}</h3>
                                        <h1>{{$slider->title}}</h1>
                                        <p>
                                            {{$slider->description}}
                                        </p>
                                        <a href="#" class="mybtn" >
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>

                                            Կարդալ Ավելին</a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-5 col-sm-offset-1 col-md-offset-3">
                                    <div class="search-box">
                                        <h3>Ի՞ՆՉ Է ՆԵՐԱՌՈՒՄ ԴԱՍԸՆԹԱՑԸ</h3>
                                        <br>
                                        <div>
                                            {!! $slider->Slider_info->title ?? ' '!!}

                                        </div>
                                <span class="btn-Register">
                                     <a href="#" class="mybtn-two"  data-toggle="modal" data-target="#signInModal"
                                        onclick="document.getElementById('slugData').value='{{$slider->slug}}'">
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                        Գրանցվել
                                    </a>
                                 </span>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </section>
    <section class="appointment-section">
        <div class="container">

            <div class="row clearfix">
                <!--Text Column-->
                <div class="text-column col-lg-7 col-md-12 col-sm-12">
                    <div class="inner">
                        <div class="sec-title">
                            <div class="section-title" style="margin: 0 0 30px;">
                                <h2>Մեծ հեռանկարային մասնագիտություն</h2>
                            </div>

                        </div>

                        <div class="content">
                            <!--Service Block-->
                            <div class="service-block-two">
                                <div class="inner-box">

                                    <h3> <a href="{{route('client.about')}}">ՄԻ ՔԻՉ ՄԵՐ ՄԱՍԻՆ</a></h3>
                                    <div>
                                        <p><span class="fa fa-check text-success"></span>
                                            Մեր ակադեմիան արդեն 3 տարի է կազմակերպում և իրականացնում է ծրագրավորման
                                            խորացված
                                            և միջազգային ստանդարտներին համապատասխան դասընթացներ՝նպատակ ունենալով
                                            աշխատաշուկային հանձնել որակյալ, գիտակ մասնագետներ:

                                        </p>
                                        <p><span class="fa fa-check text-success"></span>
                                            Պահանջարկը որակյալ կադրերի նկատմամբ՝ մենք ստեղծեցինք այս կենտրոնը, որն էլ
                                            կդառնա
                                            հաջողության գրավական աշխատանքային շուկայում:

                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--Service Block-->
                            <div class="service-block-two">
                                <div class="inner-box">

                                    <h3><a href="{{route('client.about')}}">ՄԵՐ ՍԿԶԲՈՒՆՔԵՐԸ</a></h3>
                                    <div>
                                        <p><span class="fa fa-check text-success"></span>
                                            Մենք տալիս ենք լավ աշխատանքներ՝ հիմնավոր և կայուն տեսական և գործնական
                                            գիտելիքներ
                                            ,դա է պատճառը, որ մեր շրջանավարտները աշխատանքի տեղավորման խնդիր չեն
                                            ունենում,
                                        </p>
                                        <p><span class="fa fa-check text-success"></span> Հետադարձ կապի պահպանում՝ ուսանող-դասախոս:</p>
                                        <p><span class="fa fa-check text-success"></span>
                                            Դառնալով DaniSoft Academy-ի ուսանող դուք ստանում եք ավելին․՝ հավելյալ
                                            բոնուսներ
                                            և նյութեր, խորհուրդներ, ստանում եք վիդեոդասեր, վեբինարներ որոնք առավել
                                            արդյունավետ կդարձնեն այն գիտելիքը:

                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!--Form Column-->
                <div class="form-column col-lg-5 col-md-12 col-sm-12">
                    <div class="inner wow fadeInRight animated" data-wow-delay="0ms" data-wow-duration="1500ms"
                         style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInRight;">
                        <figure class="image">
                            <br>
                            <br>
                            <br>
                            <br>
                        </figure>
                        <!--Form Box-->
                        <!--Service Block-->
                        <div class="service-block-two">
                            <div class="inner-box">

                                <h3><a href="{{route('client.about')}}">ՄԵՐ ՀԱՋՈՂՈՒԹՅՈՒՆԸ</a></h3>

                                <div>
                                    <p><span class="fa fa-check text-success"></span>
                                        Դասավանդողների մեր թիմը կազմված է փորձառու ծրագրավորողներից, ովքեր պարբերաբար
                                        վերապատրաստվում են թե՛ մասնագիտական, թե՛ դասավանդման մեթոդիկայի ժամանակակից
                                        մեթոդներին տիրապետելու համար:
                                    </p>
                                </div>
                                <br>

                            </div>
                        </div>
                        <!--Service Block-->
                        <div class="service-block-two" style="    padding: 29px 0;">
                            <div class="inner-box">

                                <h3>Համագործակցում ենք </h3>
                                <div>
                                        <p><span class="fa fa-check text-success"></span>
                                            ՏՏ Կազմակերպությունների հետ, որպեսզի դասընթացները մշակենք աշխատաշուկայի
                                            պահանջներին
                                            համպատասխան, ինչը Ձեզ հնարավորություն կտա հեշտ ինտեգրվել աշխատանքի մեջ և դառնալ
                                            պահանջված և որակյալ մասնագետ։ Դասընթացները մեծամասնությամբ կազմված են պրակտիկ
                                            բնույթի առաջադրանքներից:
                                        </p>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="populer-courses courses-bg overlay-black pd-top-100 pd-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class=" white-title">ԱՌԱՋԻԿԱ և ՆԵՐԿԱՅԻՍ ԴԱՍԸՆԹԱՑՆԵՐԻ ՄԱՍԻՆ</h2>
                    <p class="section-title white-title">ԶԱՐԳԱՑՐԵ՛Ք ՁԵՐ ՀՄՏՈՒԹՅՈՒՆՆԵՐԸ, ՁԵ՛ՌՔ ԲԵՐԵՔ ԹՐԵՆԴԱՅԻՆ
                        ՄԱՍՆԱԳԻՏՈՒԹՅՈՒՆ</p>
                </div>
            </div>
            <div class="row">
                <div class="courses-wrapper nav-control">
                @foreach($lessons as $lesson)
                    <div class="col-md-4">
                        <div class="courses-box-item">
                            <div class="course-bg" style="background-size:cover; background-image: url({{asset($lesson->avatar)}});">
                                <i class="like fa fa-heart"></i>
                                <div class="course-title">
                                    <h4>Մինջին տևողությունը</h4>
                                    <ul class="course-date">

                                        <li><i class="fa fa-clock-o"></i>{{$lesson->math}}</li>
                                    </ul>
                                </div>
                                <div class="course-teach-img">
                                    <div class="sale">
                                        {{$lesson->sale}}
                                    </div>

                                </div>
                            </div>
                            <div class="course-desc">
                                <h4>{{$lesson->title}}</h4>
                                <p>{{$lesson->description}}</p>
                                <div class="course-rating">
                                    <div class="btnreg">
                                        <div>
                                            <a href="#" class="mybtn-two" data-toggle="modal" data-target="#signInModal"
                                               onclick="document.getElementById('slugData').value='{{$lesson->slug}}'">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                Գրանցվել
                                            </a>
                                        </div>
                                        <div></div>
                                        <div>
                                            <a href="course-details.html" class="mybtn">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                Կարդալ Ավելին
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
               @endforeach
                </div>
            </div>
        </div>
    </section>
    @include('include.newStart')
    <section class="why-choose-area pd">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title">ԴԱՍԸՆԹԱՑԻ ԱՐԴՅՈՒՆՔՈՒՄ ԴՈՒՔ ԿՍՏԱՆԱՔ</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-5 lesson-result">
                    <img src="{{asset('client/assets/images/web/dasntacner.png')}}" alt="">
                </div>
                <div class="col-md-5 col-sm-6 col-md-offset-1">
                    <div class="why-choose-style-2">
                        <div class="why-choose-single-item">
                            <i class="fa fa-clock-o"></i>
                            <h4>ԲՈՆՈՒՍ</h4>
                            <ol>
                                <li><b>5000 ՀՀ ԴՐԱՄ ԱՐԺՈՂՈՒԹՅԱՄԲ ՆՎԵՐ ՔԱՐՏ`</b>
                                    <br>
                                    Այն Կարող Եք Օգտագործել DaniSoft Ակադեմիայի Այլ Դասընթացներին Մասնակցելու Համար՝ Օր
                                    WEB
                                    Ծրագրավորոմ, IT Անգլերեն, JavaScript, PHP և ԱՅԼԸՆՏՐԱՆՔԻ

                                </li>
                                <li>Եթե Դասընթացին Հրավիրեք Նաև Ձեր Ընկերոջը, Տվյալ Դասընթացի Համար Ձեզնից
                                    Յուրաքանչյուրը
                                    Կստանաք 10% ԶԵՂՉ:
                                </li>
                            </ol>
                        </div>
                        <div class="why-choose-single-item">
                            <i class="fa fa-hand-peace-o"></i>
                            <h4>CERTIFICATE</h4>
                            <p>Դասընթացի ավարտին Դուք հանձնելու եք քննություն, որի հիման վրա կստանաք Ձեր գիտելիքները
                                փաստող
                                հավաստագիր:</p>
                        </div>
                        <div class="why-choose-single-item">
                            <i class="fa fa-book"></i>
                            <h4>ԱՇԽԱՏԱՆՔ</h4>
                            <p>Աշխատանքի հնարավորություն</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@include('include.reviews')
@include('include.ourStat')
@include('include.teacher')

    <section class="blog-area pd-top-100 pd-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title">Նորություներ</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="blog-item-box">
                        <div class="blog-img">
                            <img src="{{asset('client/assets/images/blog/danisoft_and_webFoundation.png')}}" alt="">
                            <div class="blog-date">22 <span>Հուն</span></div>
                        </div>
                        <div class="text-box">
                            <h4>😇 Մենք արդեն համագործակցում ենք 𝗪𝗘𝗕 𝗙𝗼𝘂𝗻𝗱𝗮𝘁𝗶𝗼𝗻ի հետ, շտապի’ր 🤝</h4>
                            <p>Զարմանալու կարիք չկա. Danisoft-ը միշտ էլ սիրել է ուրախացնել իր ուսանողներին և մտածել
                                նրանց
                                ապագայում աշխատանքով ապահովելու մասին:
                                <a href="blog-details.html" class="read-more-btn">Կարդալ ավելին...</a></p>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="blog-item-box">
                        <div class="blog-img">
                            <img src="{{asset('client/assets/images/blog/Danisoft_student_david.jpg')}}" alt="">
                            <div class="blog-date">15 <span>Feb</span></div>
                        </div>
                        <div class="text-box">
                            <h4>💙 Եղի’ր Դավիթի պես</h4>
                            <p>👨‍💻 Դավիթը DaniSoft-ի ամենանպատակասլաց ուսանողներից է: Ընդամենը 1 ամիս է, ինչ նա միացել
                                է
                                մեր թիմին, բայց արդեն կարողացել է ուսումնասիրել PHP, Laravel և իր գիտելիքները 5%-ից
                                հասցրել
                                է 45%-ի:<a href="blog-details.html" class="read-more-btn">Contiune Reading...</a></p>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="blog-item-box">
                        <div class="blog-img">
                            <img src="{{asset('client/assets/images/blog/danisoft_learn_web_development.jpg')}}" alt="">
                            <div class="blog-date">15 <span>Feb</span></div>
                        </div>
                        <div class="text-box">
                            <h4> Ծրագրավորում սովորելը ամենաընտիր որոշումն է, որ դու կարող ես կայացնել</h4>
                            <p>Ձեզ ենք ներկայացնում մի քանի խորհուրդներ, որոնց շնորհիվ ծրագրավորում սովորելու գործընթացը
                                կդառնա էլ ավելի հաճելի և արդյունավետ: <a href="blog-details.html" class="read-more-btn">Contiune
                                    Reading...</a></p>
                            <!--  <ul>
                                <li>By Admin</li>
                                <li><i class="fa fa-heart"></i> 25</li>
                                <li><i class="fa fa-eye"></i> 200</li>
                             </ul> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('include.price')
    <section class="our-client-logo-area pd">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title">Համագործակցում ենք</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                    <div class="single-logo-item">
                        <img src="https://webfoundationllc.com/images/logo.svg" alt="">
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="single-logo-item">
                        <img src="http://agh.am/public/photos/1/58ef8b78a4dd0.PNG" alt="">
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="single-logo-item">
                        <img src="https://dgschool.am/img/mini-gray-logo.png" alt="">
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="single-logo-item">
                        <img src="{{asset('client/assets/images/company/Master.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')

@endsection
