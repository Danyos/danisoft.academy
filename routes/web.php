<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([ 'as' => 'client.', 'namespace' => 'Client'], function () {
    Route::get('/', 'SiteClentsController@index')->name('index');
    Route::get('/about', 'SiteClentsController@about')->name('about');
    Route::get('/reviews', 'SiteClentsController@reviews')->name('reviews');
    Route::get('/blog', 'SiteClentsController@blog')->name('blog');
    Route::get('/contact', 'SiteClentsController@contact')->name('contact');
    Route::get('new/start', 'SiteClentsController@newstart')->name('new_start');


    Route::resource('registers', 'RegisterLessonController');
    Route::get('lessonRegister', 'RegisterLessonController@store')->name('lessonRegister');
    Route::get('callUp', 'RegisterLessonController@CallUpCenter')->name('CallUpCenter');
    Route::get('SendMSG', 'ContactUsController@Send')->name('sendMSG');


    Route::get('/lesson', 'LessonController@lessons')->name('lesson_danisoft');
    Route::get('/price_list', 'LessonController@price')->name('price_list');
    Route::get('/lesson/{data}', 'LessonController@lessonMore')->name('lessonMore');
    Route::get('/lesson-register/{data?}', 'LessonController@lessonRegister')->name('lessonRegister');
    Route::get('gift-card', 'LessonController@GiftCard')->name('gif_card');
});

Auth::routes();

Route::redirect('/home', '/admin');
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/notes', 'HomeController@index')->name('home');
    Route::resource('LessonTimeList', 'School\LessonTimeFolderController');
    Route::post('LessonFolderUpdate', 'School\LessonTimeFolderController@updateStudent')->name('LessonFolderUpdate');
    Route::resource('StudentNewMath', 'School\StudentCreatedNewMath');
    Route::get('StudentNewMath/refresh/Student/{id}', 'School\StudentCreatedNewMath@create')->name('StudentNewMath.new.math');
    Route::get('Student/back/Student/{id}', 'School\StudentCreatedNewMath@showThisStudent')->name('StudentNewMath.showThisStudent');
    Route::resource('StudentList', 'School\StudentCreatedNewMath');
    Route::resource('StudentOfDaniSoft', 'School\StudentController');
    Route::post('LessonTimeList/createNew', 'School\LessonTimeFolderController@addLesson')->name('LessonTimeList.addLesson');
    Route::resource('notes', 'Notes\NotesController');
    Route::post('notes/update/type', 'Notes\NotesController@update')->name('notes.update.type');
    Route::get('notes/fav/type/', 'Notes\NotesController@fav')->name('notes.fav.type');

    //regiser lesson
     Route::resource('registerLesson', 'School\Reg\RegisterLessonController');
     Route::post('registerLessonDelete/{id}', 'School\Reg\RegisterLessonController@delete');
     Route::post('lesson/favorite/type/{id}', 'School\Reg\RegisterLessonController@favorite');
     Route::post('lesson/student/{id}', 'School\Reg\RegisterLessonController@student');
     Route::post('lesson/status/{id}', 'School\Reg\RegisterLessonController@status');
     Route::get('lesson/info/{id}', 'School\Reg\RegisterLessonController@info')->name('lessonReg.info');
     Route::post('lesson/info_store', 'School\Reg\RegisterLessonController@info_store')->name('lessonRegComments.info');


    //endregister

    //CallUp lesson
    Route::resource('CallUp', 'CallUp\CallUpController');
    Route::post('CallUpDelete/{id}', 'CallUp\CallUpController@delete');
    Route::post('CallUp/favorite/type/{id}', 'CallUp\CallUpController@favorite');
    Route::post('CallUp/status/{id}', 'CallUp\CallUpController@status');
    //endCallUp
    //
  //regiser lesson
    Route::resource('contact', 'Contact\ContactUsController');
    Route::post('contactDelete/{id}', 'CallUp\CallUpController@delete');
    Route::post('contact/favorite/type/{id}', 'CallUp\CallUpController@favorite');
    Route::post('contact/status/{id}', 'CallUp\CallUpController@status');
    //endregister



    Route::delete('permissions/destroy', 'User\PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'User\PermissionsController');
    Route::delete('roles/destroy', 'User\RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'User\RolesController');
    Route::delete('users/destroy', 'User\UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'User\UsersController');
});
